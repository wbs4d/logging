//%attributes = {"invisible":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: LOG TIME TO DIE

// I’ve seen things you people wouldn’t believe.
// Attack ships on fire off the shoulder of Orion.
// I watched C-beams glitter in the dark near the Tannhäuser Gate.
// All those moments will be lost in time, like tears in rain.
//
// Time to die.

// Created by Wayne Stewart (2021-08-09T14:00:00Z)
//     waynestewart@mac.com
// ----------------------------------------------------

//If (False)
//LOG TIME TO DIE
//End if 

LOG CLOSE LOG2  // This does the work of closing the logs

KILL WORKER:C1390

