//%attributes = {"invisible":true,"shared":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: LOG COMPRESS (Path to File or Folder {;Delete after compressing})

// Compresses the file passed to it in the first parameter
// If the second (optional) parameter is set to true
// the original file will be deleted after successful zipping

// Access: Private

// Parameters: 
//   $1 : Text : Full path to document or folder
//   $2 : Boolean : Detete after compressing (Default: don't compress)

// Created by Wayne Stewart (2021-09-02T14:00:00Z)

//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($Path_t : Text; $delete_b : Boolean)

var $ArchivePath_t; $DesiredProcessName_t : Text
var $Source_o; $results_o; $destination_o : Object
var $Parameters_i; $pathType_i : Integer


// ----------------------------------------------------

$Parameters_i:=Count parameters:C259
$DesiredProcessName_t:="$zip worker"

Case of 
	: ($Parameters_i=0)
		CALL WORKER:C1389(1; "LOG BUG ALERT"; "LOG COMPRESS"; "Not enough parameters")
		
	: ($Parameters_i=1)
		$delete_b:=True:C214
		
	Else 
		// They have been assigned already
		
End case 

If (Length:C16($Path_t)>0)  // Only continue if we have a path
	If (Current process name:C1392#$DesiredProcessName_t)  // Move to a new worker
		CALL WORKER:C1389($DesiredProcessName_t; "LOG COMPRESS"; $Path_t; $delete_b)
	Else 
		
		$pathType_i:=Test path name:C476($Path_t)
		
		Case of 
			: ($pathType_i=Is a document:K24:1)
				$Source_o:=File:C1566($path_t; fk platform path:K87:2)
				$ArchivePath_t:=$Path_t+".zip"
				
			: ($pathType_i=Is a folder:K24:2)
				$ArchivePath_t:=Substring:C12($Path_t; 1; Length:C16($Path_t)-1)+".zip"  // Get rid of the folder separator at the end
				$Source_o:=Folder:C1567($path_t; fk platform path:K87:2)
				
			Else 
				CALL WORKER:C1389(1; "LOG BUG ALERT"; "LOG COMPRESS"; "Incorrect file path\r"+$Path_t)
				
		End case 
		
		If ($Source_o#Null:C1517)
			
			$destination_o:=File:C1566($ArchivePath_t; fk platform path:K87:2)
			$results_o:=ZIP Create archive:C1640($Source_o; $destination_o)
			
			If ($results_o.success) & ($delete_b)  // Get rid of source document?
				If ($pathType_i=Is a document:K24:1)
					DELETE DOCUMENT:C159($Path_t)
				Else 
					DELETE FOLDER:C693($Path_t; Delete with contents:K24:24)
				End if 
			End if 
			
		End if 
		
		
	End if 
	
End if 