//%attributes = {"invisible":true,"shared":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: Log Maximum Size {(Maximum size)} --> Longint

// Allows you set the maximum size of a log file
// before it is rolled over
// Pass 0 to indicate 'do not rollover'

// Access: Shared

// Parameters: 
//   $1 : Longint : Maximum size (optional)

// Returns: 
//   $0 : Longint : Maximum size

// Created by Wayne Stewart (2021-08-09)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($maxLogSize_i : Integer) : Integer

Log_Init

If (Count parameters:C259=1)
	Use (Storage:C1525.log)
		Storage:C1525.log.maxLogSize:=$maxLogSize_i
	End use 
End if 

return Storage:C1525.log.maxLogSize