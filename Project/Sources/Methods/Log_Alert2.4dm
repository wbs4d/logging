//%attributes = {"invisible":true,"preemptive":"incapable"}
// ----------------------------------------------------
// Project Method: Log_Alert2 (Message)

// Displays a message in the Application process

// Access: Private

// Parameters: 
//   $1 : Text : The message to display

// Created by Wayne Stewart (2024-05-17T14:00:00Z)

//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($message : Text)

ALERT:C41($message)