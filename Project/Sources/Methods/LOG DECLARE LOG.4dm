//%attributes = {"invisible":true,"shared":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: LOG DECLARE LOG (Label {; File Name {; Folder Path }})

// Allows the declaration of a different log

// Access: Shared

// Parameters: 
//   $1 : Text : Log Label
//   $2 : text : Log Name
//   $3 : text : Log fileName - optional

// Created by Wayne Stewart (2018-08-04T14:00:00Z)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($Label_t : Text; $fileName_t : Text; $folderPath_t : Text)

Log_Init

var $Log_o : Object
var $Parameters_i : Integer

$Parameters_i:=Count parameters:C259

Case of 
	: ($Parameters_i=1)
		$fileName_t:=$Label_t+".txt"
		$folderPath_t:=Storage:C1525.k.logsFolder
		
	: ($Parameters_i=2)
		$folderPath_t:=Storage:C1525.k.logsFolder
		
	Else 
		// All declared
End case 

If (Storage:C1525.logs[$Label_t]=Null:C1517)  // This has not yet been declared
	$Log_o:=New shared object:C1526("folderPath"; $folderPath_t; "fileName"; $fileName_t)
	
	Use (Storage:C1525.logs)
		Storage:C1525.logs[$Label_t]:=$Log_o
	End use 
	
	
End if 