//%attributes = {"invisible":true}

// m2bBUILD
// Created by Wayne Stewart (2021-08-26T14:00:00Z)
//  Method is an autostart type
//     waynestewart@mac.com
// ----------------------------------------------------

C_LONGINT:C283($ProcessID_i; $StackSize_i)
C_TIME:C306($buildsettings_h)
C_TEXT:C284($buildSettingsPath_t; $buildSettings_t; $DesiredProcessName_t; $element_t; $elementRef_t; $Form_t; $rootNode_t; $BuiltStructure_t)
C_TEXT:C284($ProjectFolder_t; $buildFolder_t; $templatePath_t)
// ----------------------------------------------------

$StackSize_i:=0
$Form_t:=""
$DesiredProcessName_t:=Current method name:C684

If (Current process name:C1392=$DesiredProcessName_t)
	
	// Set the destination
	$ProjectFolder_t:=Get 4D folder:C485(Database folder:K5:14)
	$buildFolder_t:=$ProjectFolder_t+"Builds"+Folder separator:K24:12
	CREATE FOLDER:C475($buildFolder_t; *)
	
	$buildSettingsPath_t:=$ProjectFolder_t+"Settings"+Folder separator:K24:12+"buildApp.4DSettings"
	
	If (Test path name:C476($buildSettingsPath_t)=Is a document:K24:1)  // Check it exists
	Else 
		$templatePath_t:=Get 4D folder:C485(Current resources folder:K5:16)+"buildApp.4DSettings"
		COPY DOCUMENT:C541($templatePath_t; $buildSettingsPath_t)
	End if 
	
	$buildsettings_h:=Open document:C264($buildSettingsPath_t)
	If (OK=1)
		
		RECEIVE PACKET:C104($buildsettings_h; $buildSettings_t; MAXTEXTLENBEFOREV11:K35:3)
		CLOSE DOCUMENT:C267($buildsettings_h)
		$rootNode_t:=DOM Parse XML variable:C720($buildSettings_t; False:C215)
		
		// Set the version
		$element_t:="/Preferences4D/BuildApp/BuildApplicationName"
		$elementRef_t:=DOM Find XML element:C864($rootNode_t; $element_t)
		DOM GET XML ELEMENT VALUE:C731($elementRef_t; $BuiltStructure_t)
		$BuiltStructure_t:="Logging.4dbase"
		DOM SET XML ELEMENT VALUE:C868($elementRef_t; $BuiltStructure_t)
		
		// Set the destination folder
		$element_t:="/Preferences4D/BuildApp/"
		If (Is macOS:C1572)
			$element_t:=$element_t+"BuildMacDestFolder"
		Else 
			$element_t:=$element_t+"BuildWinDestFolder"
		End if 
		$elementRef_t:=DOM Find XML element:C864($rootNode_t; $element_t)
		DOM SET XML ELEMENT VALUE:C868($elementRef_t; $buildFolder_t)
		
		// Save the settings
		DOM EXPORT TO FILE:C862($rootNode_t; $buildSettingsPath_t)
		DOM CLOSE XML:C722($rootNode_t)
		
		ON ERR CALL:C155("LOG SILENT ERROR")
		BUILD APPLICATION:C871($buildSettingsPath_t)
		
		
		If (OK=1)
			ALERT:C41("Build Succeeded")
			SHOW ON DISK:C922($buildFolder_t+"Compiled Database"+Folder separator:K24:12; *)
		Else 
			SHOW ON DISK:C922(Get 4D folder:C485(Logs folder:K5:19); *)
		End if 
		ON ERR CALL:C155("")
		
	End if 
	
Else 
	// This version allows for any number of processes
	// $ProcessID_i:=New Process(Current method name;$StackSize_i;$DesiredProcessName_t)
	
	// On the other hand, this version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684; $StackSize_i; $DesiredProcessName_t; *)
	
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 