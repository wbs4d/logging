//%attributes = {"invisible":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: LOG THIS (Log entry; Log folder; Log file name)

// Writes the current log entry into the document
// If the size exceeds the max size the log will be rolled over

// Access: Private

// Parameters: 
//   $1 : Text : Log entry
//   $2 : Text : Log folder
//   $2 : Text : Log file name

// Created by Wayne Stewart (2021-08-08T14:00:00Z)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($logEntry_t : Text; $PathToLogFolder_t : Text; $LogFileName_t : Text)

var $TS_t : Text

var $PathToLogFile_t; $ErrorHandler_t; $extension_t; $archivedfileName_t; $archivedfilePath_t : Text
var $docRef_h : Time
var $Position_i; $dot_i : Integer

Log_Init

$ErrorHandler_t:=Method called on error:C704

ON ERR CALL:C155("LOG SILENT ERROR")
If (Position:C15($LogFileName_t; $PathToLogFile_t)=0)  // If they have added the file name to the path, don't do this
	$PathToLogFile_t:=$PathToLogFolder_t+$LogFileName_t
End if 

$Position_i:=Find in array:C230(Log_Paths_at; $PathToLogFile_t)
If ($Position_i>0)
	$docRef_h:=Log_DocRefs_ah{$Position_i}
	
Else 
	
	If (Test path name:C476($PathToLogFile_t)=Is a document:K24:1)
		$docRef_h:=Append document:C265($PathToLogFile_t)
	Else 
		If (Test path name:C476($PathToLogFolder_t)#Is a folder:K24:2)
			CREATE FOLDER:C475($PathToLogFolder_t; *)
		End if 
		$docRef_h:=Create document:C266($PathToLogFile_t; "TEXT")
		
	End if 
	
	APPEND TO ARRAY:C911(Log_Paths_at; $PathToLogFile_t)
	APPEND TO ARRAY:C911(Log_DocRefs_ah; $docRef_h)
	$Position_i:=Size of array:C274(Log_DocRefs_ah)  // In case we need it below (very unlikely)
	
End if 

If (OK=1)
	SEND PACKET:C103($docRef_h; $logEntry_t)
	If (Storage:C1525.log.maxLogSize#0)\
		 & (Get document size:C479($PathToLogFile_t)>Storage:C1525.log.maxLogSize)
		
		CLOSE DOCUMENT:C267($docRef_h)
		
		$dot_i:=logUtil_PositionR("."; $LogFileName_t)
		If ($dot_i=0)
			$extension_t:=""
			$archivedfileName_t:=$LogFileName_t
		Else 
			$extension_t:=Substring:C12($LogFileName_t; $dot_i+1)
			$archivedfileName_t:=Substring:C12($LogFileName_t; 1; $dot_i-1)
		End if 
		
		$TS_t:=Replace string:C233(Timestamp:C1445; ":"; "")
		$TS_t:=Replace string:C233($TS_t; "-"; "")
		$TS_t:=Substring:C12($TS_t; 1; 15)
		
		$archivedfileName_t:=$archivedfileName_t+"_"+$TS_t+$extension_t
		$archivedfilePath_t:=$PathToLogFolder_t+$archivedfileName_t
		
		MOVE DOCUMENT:C540($PathToLogFile_t; $archivedfilePath_t)
		
		$docRef_h:=Create document:C266($PathToLogFile_t; "TEXT")  // Create a new document
		Log_DocRefs_ah{$Position_i}:=$docRef_h  // Update the array
		LOG COMPRESS($archivedfilePath_t)  //  Now compress the old file
		
	End if 
	
	//
End if 


ON ERR CALL:C155($ErrorHandler_t)