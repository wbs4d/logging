//%attributes = {"invisible":true,"shared":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: Log Enable({enable}) --> Boolean

// Allows the developer to turn the logger on or off.  Also returns True
// if logging is currently enabled.

// Access: Shared

// Parameters:
// $1 : Boolean : Turn on logging (optional)

// Returns:
// $0 : Boolean : True if logging

// Created by Dave Batton on Dec 27, 2004
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($enabled_b : Boolean) : Boolean

Log_Init

If (Count parameters:C259>=1)
	lg.enabled:=$enabled_b
End if 

return lg.enabled