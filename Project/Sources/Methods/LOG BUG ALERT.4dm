//%attributes = {"invisible":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: LOG BUG ALERT (Bug Details in Object or Method with error {; More Details {; Line number {; Display Alert}}})

// Use this method to report on an unexpected event in your code
//  (Missing parameter etc) 
//  You may either
//  1. Pass four simple parameters - Method Name, Error Description, Line number, and Display Alert
//  OR
//  2. Pass an object with those parameters embedded

// A planned enhancement but not written yet would be to log the error

// Access: Shared

// Parameters: 
//   $var : Variant : Text (The method with the error) or Object
//   $var.methodName : Text : The method with the error
//   $var.errorDetails : Text : A description of the error
//   $var.lineNumber : Number : The line number
//   $var.displayAlert : Boolean : Display alert
//   $var.logError : Object : Not implemented

//   $2 : Text : A description of the error (optional)
//   $lineNumber_i : Longint : Line Number (optional)
//   $4 : Boolean : Display Alert (optional)

// Created by Wayne Stewart (2021-08-05T14:00:00Z)

//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($var : Variant; $details_t : Text; $lineNumber_i : Integer; $displayAlert_b : Boolean)

//If (False)
//C_VARIANT(LOG BUG ALERT; $var)
//C_TEXT(LOG BUG ALERT; $2)
//C_LONGINT(LOG BUG ALERT; $lineNumber_i)
//C_BOOLEAN(LOG BUG ALERT; $4)
//End if 

var $lineNumber_t; $errorMethod_t; $Alert_t; $CR; $tab : Text

var $isPreemptive : Boolean


$CR:=Char:C90(13)
$tab:=Char:C90(9)

Case of 
	: (Count parameters:C259=4)
		$lineNumber_t:=String:C10($lineNumber_i)
		$errorMethod_t:=$var
		
	: (Count parameters:C259=3)
		$lineNumber_t:=String:C10($lineNumber_i)
		$errorMethod_t:=$var
		$displayAlert_b:=True:C214
		
	: (Count parameters:C259=2)  // This is probably the most often called version
		$errorMethod_t:=$var
		$displayAlert_b:=True:C214
		
	: (Value type:C1509($var)=Is text:K8:3)
		$errorMethod_t:=$var
		$displayAlert_b:=True:C214
		
	: (Value type:C1509($var)=Is object:K8:27)
		$errorMethod_t:=$var.methodName
		$details_t:=$var.errorDetails
		If ($var.lineNumber#Null:C1517)
			$lineNumber_t:=String:C10($var.lineNumber)
		End if 
		If ($var.displayAlert#Null:C1517)
			$lineNumber_t:=$var.displayAlert
		Else 
			$displayAlert_b:=True:C214
		End if 
		
		
End case 

$Alert_t:="ERROR IN METHOD: "+$errorMethod_t
If (Length:C16($lineNumber_t)#0)
	$Alert_t:=$Alert_t+$CR+"Line: "+$lineNumber_t
End if 

If (Length:C16($details_t)#0)
	$Alert_t:=$Alert_t+$CR+"Description: "+$details_t
End if 


If ($displayAlert_b)
	CALL WORKER:C1389(1; "Log_Alert2"; $Alert_t)
End if 


$Alert_t:=Replace string:C233($Alert_t; $CR; $tab)

LOG EVENT:C667(Into 4D commands log:K38:7; $Alert_t)
LOG EVENT:C667(Into 4D debug message:K38:5; $Alert_t)


//var $processName : Text
//var $state; $time; $flags : Integer
//PROCESS PROPERTIES(Current process; $processName; $state; $time; $flags)
//$isPreemptive:=($flags ?? 1)

//If ($isPreemptive)
//Else 
////%T-
//TRACE
////%T+
//End if 



// You may want to comment out this
TRACE:C157

