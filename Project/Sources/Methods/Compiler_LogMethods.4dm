//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Compiler_LogMethods

// Just the methods

// Access: Private

// Created by Wayne Stewart (2018-09-03T14:00:00Z)
//     waynestewart@mac.com
// ----------------------------------------------------


// Parameters
If (False:C215)  // So we never run this as code.
	C_TEXT:C284(LOG ADD ENTRY; ${1})
	C_TEXT:C284(LOG CLOSE LOG; $1)
	C_TEXT:C284(LOG CLOSE LOG2; $1)
	C_TEXT:C284(LOG DECLARE LOG; $1; $2; $3)
	C_BOOLEAN:C305(LOG ENABLE; $0; $1)
	C_TEXT:C284(Log File Name; $0; $1)
	C_TEXT:C284(Log Folder Path; $1; $0)
	C_TEXT:C284(Log Form Event; $0)
	C_LONGINT:C283(LOG STOP LOG WRITER2; $1)
	C_TEXT:C284(LOG THIS; $1; $2; $3)
	C_TEXT:C284(LOG USE LOG; $1)
	C_TEXT:C284(STRESS TEST; $1)
	C_TEXT:C284(LOG COMPRESS; $1)
	C_BOOLEAN:C305(LOG COMPRESS; $2)
	C_LONGINT:C283(Log Maximum Size; $1; $0)
	
	C_TEXT:C284(logUtil_PositionR; $1; $2)
	C_LONGINT:C283(logUtil_PositionR; $0)
	C_TEXT:C284(logUtil_GetFileName; $0; $1)
	
	C_TEXT:C284(Log Current Log; $0)
	
	C_VARIANT:C1683(LOG BUG ALERT; $1)
	C_TEXT:C284(LOG BUG ALERT; $2)
	C_LONGINT:C283(LOG BUG ALERT; $3)
	C_BOOLEAN:C305(LOG BUG ALERT; $4)
	
	
	C_TEXT:C284(Log ISO Time Stamp; $0)
	C_DATE:C307(Log ISO Time Stamp; $1)
	C_TIME:C306(Log ISO Time Stamp; $2)
	C_BOOLEAN:C305(Log ISO Time Stamp; $3)
	
	C_TEXT:C284(UTIL_WriteMethodComments; $1)
	C_BOOLEAN:C305(UTIL_WriteMethodComments; $2; $3)
	C_TEXT:C284(UTIL_BuildComponent; $1)
	
	C_TEXT:C284(Log_Alert2; $1)
	C_TEXT:C284(util_Get4DVersion; $0)
	
End if 