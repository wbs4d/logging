//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Log Form Event --> Text

// $formEvent_t:=s Form event Code

// Access: Shared

// $formEvent_t:=s: 
//   $0 : Type : Description

// Created by Wayne Stewart (2020-04-24T14:00:00Z)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE()->$formEvent_t : Text

var $formEventCode_i : Integer

$formEventCode_i:=Form event code:C388

Case of 
		
	: ($formEventCode_i=1)
		$formEvent_t:="On Load"
		
	: ($formEventCode_i=2)
		$formEvent_t:="On Mouse Up"
		
	: ($formEventCode_i=3)
		$formEvent_t:="On Validate"
		
	: ($formEventCode_i=4)
		$formEvent_t:="On Clicked"
		
	: ($formEventCode_i=5)
		$formEvent_t:="On Header"
		
	: ($formEventCode_i=6)
		$formEvent_t:="On Printing Break"
		
	: ($formEventCode_i=7)
		$formEvent_t:="On Printing Footer"
		
	: ($formEventCode_i=8)
		$formEvent_t:="On Display Detail"
		
	: ($formEventCode_i=9)
		$formEvent_t:="On VP Ready"
		
	: ($formEventCode_i=10)
		$formEvent_t:="On Outside Call"
		
	: ($formEventCode_i=11)
		$formEvent_t:="On Activate"
		
	: ($formEventCode_i=12)
		$formEvent_t:="On Deactivate"
		
	: ($formEventCode_i=13)
		$formEvent_t:="On Double Clicked"
		
	: ($formEventCode_i=14)
		$formEvent_t:="On Losing Focus"
		
	: ($formEventCode_i=15)
		$formEvent_t:="On Getting Focus"
		
	: ($formEventCode_i=16)
		$formEvent_t:="On Drop"
		
	: ($formEventCode_i=17)
		$formEvent_t:="On Before Keystroke"
		
	: ($formEventCode_i=18)
		$formEvent_t:="On Menu Selected"
		
	: ($formEventCode_i=19)
		$formEvent_t:="On Plug in Area"
		
	: ($formEventCode_i=21)
		$formEvent_t:="On Drag Over"
		
	: ($formEventCode_i=22)
		$formEvent_t:="On Close Box"
		
	: ($formEventCode_i=23)
		$formEvent_t:="On Printing Detail"
		
	: ($formEventCode_i=24)
		$formEvent_t:="On Unload"
		
	: ($formEventCode_i=25)
		$formEvent_t:="On Open Detail"
		
	: ($formEventCode_i=26)
		$formEvent_t:="On Close Detail"
		
	: ($formEventCode_i=27)
		$formEvent_t:="On Timer"
		
	: ($formEventCode_i=28)
		$formEvent_t:="On After Keystroke"
		
	: ($formEventCode_i=29)
		$formEvent_t:="On Resize"
		
	: ($formEventCode_i=30)
		$formEvent_t:="On After Sort"
		
	: ($formEventCode_i=31)
		$formEvent_t:="On Selection Change"
		
	: ($formEventCode_i=32)
		$formEvent_t:="On Column Moved"
		
	: ($formEventCode_i=33)
		$formEvent_t:="On Column Resize"
		
	: ($formEventCode_i=34)
		$formEvent_t:="On Row Moved"
		
	: ($formEventCode_i=35)
		$formEvent_t:="On Mouse Enter"
		
	: ($formEventCode_i=36)
		$formEvent_t:="On Mouse Leave"
		
	: ($formEventCode_i=37)
		$formEvent_t:="On Mouse Move"
		
	: ($formEventCode_i=38)
		$formEvent_t:="On Alternative Click"
		
	: ($formEventCode_i=39)
		$formEvent_t:="On Long Click"
		
	: ($formEventCode_i=40)
		$formEvent_t:="On Load Record"
		
	: ($formEventCode_i=41)
		$formEvent_t:="On Before Data Entry"
		
	: ($formEventCode_i=42)
		$formEvent_t:="On Header Click"
		
	: ($formEventCode_i=43)
		$formEvent_t:="On Expand"
		
	: ($formEventCode_i=44)
		$formEvent_t:="On Collapse"
		
	: ($formEventCode_i=45)
		$formEvent_t:="On After Edit"
		
	: ($formEventCode_i=46)
		$formEvent_t:="On Begin Drag Over"
		
	: ($formEventCode_i=47)
		$formEvent_t:="On Begin URL Loading"
		
	: ($formEventCode_i=48)
		$formEvent_t:="On URL Resource Loading"
		
	: ($formEventCode_i=49)
		$formEvent_t:="On End URL Loading"
		
	: ($formEventCode_i=50)
		$formEvent_t:="On URL Loading Error"
		
	: ($formEventCode_i=51)
		$formEvent_t:="On URL Filtering"
		
	: ($formEventCode_i=52)
		$formEvent_t:="On Open External Link"
		
	: ($formEventCode_i=53)
		$formEvent_t:="On Window Opening Denied"
		
	: ($formEventCode_i=54)
		$formEvent_t:="On bound variable change"
		
	: ($formEventCode_i=56)
		$formEvent_t:="On Page Change"
		
	: ($formEventCode_i=57)
		$formEvent_t:="On Footer Click"
		
	: ($formEventCode_i=58)
		$formEvent_t:="On Delete Action"
		
	: ($formEventCode_i=59)
		$formEvent_t:="On Scroll"
		
	: ($formEventCode_i=60)
		$formEvent_t:="On Row Resize"
		
	Else 
		
		$formEvent_t:="Unknown Form Event: "+String:C10($formEventCode_i)
		
End case 

