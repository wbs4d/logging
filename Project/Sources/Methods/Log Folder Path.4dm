//%attributes = {"invisible":true,"shared":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: Log Folder Path {(The folder the log file is saved in)} --> Text

// This method retrieves the folder the log file is saved in
// You can also use this method to set the folder path

// Access: Shared

// Parameters: 
//   $1 : Text : Folder Path (optional)

// Returns: 
//   $0 : Text : Folder Path

// Created by Wayne Stewart (2018-10-29T13:00:00Z)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($logPath_t : Text) : Text

Log_Init

If (Count parameters:C259=1)
	lg.folder:=$logPath_t
End if 

If (Length:C16(lg.folder)=0)
	lg.folder:=Storage:C1525.k.logsFolder  // Reset to default
End if 

return lg.folder

