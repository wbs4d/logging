//%attributes = {"invisible":true,"shared":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: Log File Name {(Log Name)} --> Text

// Allows you set the current log file name
// Always returns the current log file name

// Access: Shared

// Parameters: 
//   $1 : TEXT : The log file name (optional)

// Returns: 
//   $0 : TEXT : The log file name

// Created by Wayne Stewart (2018-10-29T13:00:00Z)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($logName_t : Text) : Text

Log_Init

If (Count parameters:C259=1)
	lg.file:=$logName_t
End if 

If (Length:C16(lg.file)=0)
	lg.file:=Storage:C1525.k.defaultLog  // Reset to default
End if 


return lg.file

