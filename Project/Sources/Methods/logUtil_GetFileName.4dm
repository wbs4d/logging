//%attributes = {"invisible":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: log_GetFileName (path) --> Text

// Returns the file name from the full pathname.

// Access Type: Shared

// Parameters: 
//   $1 : Text : A full pathname

// Returns: 
//   $0 : Text : The file name at the end of the pathname (without the extension)

// Created by Wayne Stewart (2021-08-09)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($path_t : Text) : Text
var $File_o : Object

$File_o:=File:C1566($path_t)

return Substring:C12($File_o.name; (logUtil_PositionR("/"; $File_o.name)+1))