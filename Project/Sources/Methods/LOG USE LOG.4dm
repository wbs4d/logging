//%attributes = {"invisible":true,"shared":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: LOG USE LOG {(Log Label)}

// Pass the log label to use,
// if you call this without a parameter, then the default log will be used

// Access: Shared

// Parameters: 
//   $1 : Text : The Log to use or nothing to return to default


// Created by Wayne Stewart (2018-08-04T14:00:00Z)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($logLabel_t : Text)
var $Temp_o : Object

If (Count parameters:C259=1)
	
	If (Length:C16($logLabel_t)>0)
		LOG DECLARE LOG($logLabel_t)  // This will do no harm if the log has already been declared
		
		$Temp_o:=Storage:C1525.logs[$logLabel_t]
		lg.file:=$Temp_o.fileName
		lg.folder:=$Temp_o.folderPath
		
	Else 
		lg.file:=Storage:C1525.k.defaultLog
		lg.folder:=Storage:C1525.k.logsFolder
		
	End if 
	
Else 
	lg.file:=Storage:C1525.k.defaultLog
	lg.folder:=Storage:C1525.k.logsFolder
	
End if 