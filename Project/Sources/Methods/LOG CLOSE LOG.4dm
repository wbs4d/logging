//%attributes = {"invisible":true,"shared":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: LOG CLOSE LOG {(Log Label)}

// Pass the log label to save and close that log
// If you call this without any parameters all the logs will be closed and saved

// Access: Shared

// Parameters: 
//   $1 : Text : The log label (optional)

// Created by Wayne Stewart (2018-08-05T14:00:00Z)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE($logLabel_t : Text)

If (Count parameters:C259=1)
	CALL WORKER:C1389(Log Writer; "LOG CLOSE LOG2"; $logLabel_t)
Else 
	CALL WORKER:C1389(Log Writer; "LOG CLOSE LOG2")
End if 



