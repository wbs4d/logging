//%attributes = {"invisible":true,"preemptive":"capable"}
/* ----------------------------------------------------
  Project Method: Compiler_Log

  Compiler variables related to the Foundation Log routines.

  Access: Private

  Parameters: None

  Returns: Nothing

  Created by Dave Batton on Jul 26, 2003
WBS 21/1/2025: Update to new syntax

*/

var lg : Object

If (lg.initialised=Null:C1517)
	ARRAY TEXT:C222(Log_Paths_at; 0)
	ARRAY TIME:C1223(Log_DocRefs_ah; 0)
	
End if 

