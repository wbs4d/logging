//%attributes = {"invisible":true,"shared":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: Log Current Log --> Current Log

// Returns the current log

// Access: Shared

// Returns: 
//   $0 : Text : The current log

// Created by Wayne Stewart (2021-08-13)
//     waynestewart@mac.com
// WBS 21/1/2025: Update to new syntax
// ----------------------------------------------------

#DECLARE() : Text

Log_Init

return lg.file
