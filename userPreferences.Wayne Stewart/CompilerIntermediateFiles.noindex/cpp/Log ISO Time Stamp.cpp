extern Txt K;
extern Txt K00;
extern Txt K000;
extern Txt K0000;
extern Txt KT;
extern Txt K_2D;
extern Txt K_2E;
extern Txt K_3A;
extern unsigned char D_proc_LOG_20ISO_20TIME_20STAMP[];
void proc_LOG_20ISO_20TIME_20STAMP( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20ISO_20TIME_20STAMP);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt lseconds__t;
		Txt lminutes__t;
		Txt lhours__t;
		Txt ltime__t;
		new ( outResult) Txt();
		{
			Long t0;
			t0=inNbExplicitParam;
			if (0!=t0.get()) goto _3;
		}
		{
			Date t2;
			c.f.fLine=32;
			if (g->Call(ctx,(PCV[]){t2.cv()},0,33)) goto _0;
			Parm<Date>(ctx,inParams,inNbParam,1)=t2.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Time t3;
			c.f.fLine=33;
			if (g->Call(ctx,(PCV[]){t3.cv()},0,178)) goto _0;
			Parm<Time>(ctx,inParams,inNbParam,2)=t3.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=34;
		Parm<Bool>(ctx,inParams,inNbParam,3)=Bool(1).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_3:
		{
			Long t4;
			t4=inNbExplicitParam;
			if (1!=t4.get()) goto _4;
		}
		{
			Time t6;
			c.f.fLine=37;
			if (g->Call(ctx,(PCV[]){t6.cv()},0,178)) goto _0;
			Parm<Time>(ctx,inParams,inNbParam,2)=t6.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=38;
		Parm<Bool>(ctx,inParams,inNbParam,3)=Bool(1).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_4:
		{
			Long t7;
			t7=inNbExplicitParam;
			if (2!=t7.get()) goto _5;
		}
		c.f.fLine=41;
		Parm<Bool>(ctx,inParams,inNbParam,3)=Bool(1).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_5:
		{
			Long t9;
			t9=inNbExplicitParam;
			if (3!=t9.get()) goto _6;
		}
		c.f.fLine=44;
		Parm<Bool>(ctx,inParams,inNbParam,3)=Bool(0).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_6:
_2:
		{
			Date t11;
			c.f.fLine=48;
			t11=Parm<Date>(ctx,inParams,inNbParam,1).get();
			Long t12;
			if (g->Call(ctx,(PCV[]){t12.cv(),t11.cv()},1,25)) goto _0;
			Txt t13;
			if (g->Call(ctx,(PCV[]){t13.cv(),t12.cv(),K0000.cv()},2,10)) goto _0;
			Res<Txt>(outResult)=t13.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t14;
			c.f.fLine=49;
			g->AddString(Res<Txt>(outResult).get(),K_2D.get(),t14.get());
			Date t15;
			t15=Parm<Date>(ctx,inParams,inNbParam,1).get();
			Long t16;
			if (g->Call(ctx,(PCV[]){t16.cv(),t15.cv()},1,24)) goto _0;
			Txt t17;
			if (g->Call(ctx,(PCV[]){t17.cv(),t16.cv(),K00.cv()},2,10)) goto _0;
			g->AddString(t14.get(),t17.get(),Res<Txt>(outResult).get());
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t19;
			c.f.fLine=50;
			g->AddString(Res<Txt>(outResult).get(),K_2D.get(),t19.get());
			Date t20;
			t20=Parm<Date>(ctx,inParams,inNbParam,1).get();
			Long t21;
			if (g->Call(ctx,(PCV[]){t21.cv(),t20.cv()},1,23)) goto _0;
			Txt t22;
			if (g->Call(ctx,(PCV[]){t22.cv(),t21.cv(),K00.cv()},2,10)) goto _0;
			g->AddString(t19.get(),t22.get(),Res<Txt>(outResult).get());
		}
		if (ctx->doingAbort) goto _0;
		{
			Long t24;
			t24=inNbExplicitParam;
			if (1==t24.get()) goto _7;
		}
		{
			Bool t26;
			c.f.fLine=53;
			t26=Parm<Date>(ctx,inParams,inNbParam,1).get()==Date(0,0,0).get();
			if (!(t26.get())) goto _8;
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=54;
		Res<Txt>(outResult)=K.get();
		goto _9;
_8:
		c.f.fLine=56;
		g->AddString(Res<Txt>(outResult).get(),KT.get(),Res<Txt>(outResult).get());
_9:
		{
			Time t28;
			c.f.fLine=59;
			t28=Parm<Time>(ctx,inParams,inNbParam,2).get();
			Txt t29;
			if (g->Call(ctx,(PCV[]){t29.cv(),t28.cv(),Long(1).cv()},2,10)) goto _0;
			ltime__t=t29.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t30;
			c.f.fLine=60;
			if (g->Call(ctx,(PCV[]){t30.cv(),ltime__t.cv(),Long(1).cv(),Long(2).cv()},3,12)) goto _0;
			lhours__t=t30.get();
		}
		{
			Txt t31;
			c.f.fLine=61;
			if (g->Call(ctx,(PCV[]){t31.cv(),ltime__t.cv(),Long(4).cv(),Long(2).cv()},3,12)) goto _0;
			lminutes__t=t31.get();
		}
		{
			Txt t32;
			c.f.fLine=62;
			if (g->Call(ctx,(PCV[]){t32.cv(),ltime__t.cv(),Long(7).cv(),Long(2).cv()},3,12)) goto _0;
			lseconds__t=t32.get();
		}
		{
			Txt t33;
			c.f.fLine=63;
			g->AddString(Res<Txt>(outResult).get(),lhours__t.get(),t33.get());
			Txt t34;
			g->AddString(t33.get(),K_3A.get(),t34.get());
			Txt t35;
			g->AddString(t34.get(),lminutes__t.get(),t35.get());
			Txt t36;
			g->AddString(t35.get(),K_3A.get(),t36.get());
			g->AddString(t36.get(),lseconds__t.get(),Res<Txt>(outResult).get());
		}
_7:
		if (!(Parm<Bool>(ctx,inParams,inNbParam,3).get())) goto _10;
		if (ctx->doingAbort) goto _0;
		{
			Txt t38;
			c.f.fLine=67;
			g->AddString(Res<Txt>(outResult).get(),K_2E.get(),t38.get());
			Long t39;
			if (g->Call(ctx,(PCV[]){t39.cv()},0,459)) goto _0;
			Long t40;
			t40=t39.get()%1000;
			Txt t41;
			if (g->Call(ctx,(PCV[]){t41.cv(),t40.cv(),K000.cv()},2,10)) goto _0;
			g->AddString(t38.get(),t41.get(),Res<Txt>(outResult).get());
		}
_10:
		goto _0;
_0:
_1:
;
	}

}
