extern Txt K_2F;
extern Txt Kname;
Asm4d_Proc proc_LOGUTIL__POSITIONR;
extern unsigned char D_proc_LOGUTIL__GETFILENAME[];
void proc_LOGUTIL__GETFILENAME( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOGUTIL__GETFILENAME);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Obj lFile__o;
		new ( outResult) Txt();
		c.f.fLine=22;
		{
			Obj t0;
			if (g->Call(ctx,(PCV[]){t0.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv()},1,1566)) goto _0;
			g->Check(ctx);
			lFile__o=t0.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t1;
			c.f.fLine=24;
			if (g->GetMember(ctx,lFile__o.cv(),Kname.cv(),t1.cv())) goto _0;
			Variant t2;
			if (g->GetMember(ctx,lFile__o.cv(),Kname.cv(),t2.cv())) goto _0;
			Txt t3;
			if (!g->GetValue(ctx,(PCV[]){t3.cv(),t2.cv(),nullptr})) goto _0;
			Txt t4;
			t4=K_2F.get();
			Long t5;
			proc_LOGUTIL__POSITIONR(glob,ctx,2,2,(PCV[]){t4.cv(),t3.cv()},t5.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			Long t6;
			t6=t5.get()+1;
			Txt t7;
			if (!g->GetValue(ctx,(PCV[]){t7.cv(),t1.cv(),nullptr})) goto _0;
			Variant t8;
			if (g->Call(ctx,(PCV[]){t8.cv(),t7.cv(),t6.cv()},2,12)) goto _0;
			Txt t9;
			if (!g->GetValue(ctx,(PCV[]){t9.cv(),t8.cv(),nullptr})) goto _0;
			Res<Txt>(outResult)=t9.get();
		}
		goto _0;
_0:
_1:
;
	}

}
