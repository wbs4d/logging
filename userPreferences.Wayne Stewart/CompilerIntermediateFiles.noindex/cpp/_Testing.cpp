extern Txt K20_2E0_20_5B;
extern Txt K_5D;
Asm4d_Proc proc_UTIL__BUILDCOMPONENT;
Asm4d_Proc proc_UTIL__GET4DVERSION;
extern unsigned char D_proc___TESTING[];
void proc___TESTING( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc___TESTING);
	if (!ctx->doingAbort && c.f.fLine==0) {
		{
			Txt t0;
			c.f.fLine=3;
			proc_UTIL__GET4DVERSION(glob,ctx,0,0,nullptr,t0.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			Txt t1;
			g->AddString(K20_2E0_20_5B.get(),t0.get(),t1.get());
			Txt t2;
			g->AddString(t1.get(),K_5D.get(),t2.get());
			proc_UTIL__BUILDCOMPONENT(glob,ctx,1,1,(PCV[]){t2.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
_0:
_1:
;
	}

}
