extern Txt KLog_20Time_20to_20Die;
extern Txt KLog_20Writer;
extern Txt kmySWIhHVNTM;
extern unsigned char D_proc_LOG_20STOP_20LOG_20WRITER2[];
void proc_LOG_20STOP_20LOG_20WRITER2( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20STOP_20LOG_20WRITER2);
	if (!ctx->doingAbort && c.f.fLine==0) {
		{
			Long t0;
			t0=inNbExplicitParam;
			if (1!=t0.get()) goto _2;
		}
		c.f.fLine=6;
		if (g->Call(ctx,(PCV[]){nullptr,KLog_20Writer.cv(),KLog_20Time_20to_20Die.cv()},2,1389)) goto _0;
		g->Check(ctx);
		goto _3;
_2:
		c.f.fLine=9;
		if (g->Call(ctx,(PCV[]){nullptr,KLog_20Writer.cv(),kmySWIhHVNTM.cv(),Long(0).cv()},3,1389)) goto _0;
		g->Check(ctx);
_3:
_0:
_1:
;
	}

}
