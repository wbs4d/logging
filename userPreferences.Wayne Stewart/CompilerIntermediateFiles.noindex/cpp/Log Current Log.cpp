extern int32_t vlg;
extern Txt Kfile;
Asm4d_Proc proc_LOG__INIT;
extern unsigned char D_proc_LOG_20CURRENT_20LOG[];
void proc_LOG_20CURRENT_20LOG( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20CURRENT_20LOG);
	if (!ctx->doingAbort && c.f.fLine==0) {
		new ( outResult) Txt();
		c.f.fLine=18;
		proc_LOG__INIT(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Variant t0;
			c.f.fLine=20;
			if (g->GetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),t0.cv())) goto _0;
			Txt t1;
			if (!g->GetValue(ctx,(PCV[]){t1.cv(),t0.cv(),nullptr})) goto _0;
			Res<Txt>(outResult)=t1.get();
		}
		goto _0;
_0:
_1:
;
	}

}
