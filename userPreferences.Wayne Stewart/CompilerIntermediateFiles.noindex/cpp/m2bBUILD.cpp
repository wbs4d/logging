extern int32_t vOK;
extern Txt K;
extern Txt KBuild_20Succeeded;
extern Txt KBuilds;
extern Txt KLogging_2E4dbase;
extern Txt KSettings;
extern Txt K_3A;
extern Txt kI_UxMf2Gtu0;
extern Txt kLa2ChQdcjpU;
extern Txt kT0zyqtJ3KXg;
extern Txt kbmoXZlMq9pA;
extern Txt kjKDCQGtjyBY;
extern Txt kmScCWfEpPp4;
extern Txt kulSCpLl7Du4;
extern unsigned char D_proc_M2BBUILD[];
void proc_M2BBUILD( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_M2BBUILD);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt ltemplatePath__t;
		Txt lbuildFolder__t;
		Txt lProjectFolder__t;
		Txt lBuiltStructure__t;
		Txt lrootNode__t;
		Txt lForm__t;
		Txt lelementRef__t;
		Txt lelement__t;
		Txt lDesiredProcessName__t;
		Txt lbuildSettings__t;
		Txt lbuildSettingsPath__t;
		Time lbuildsettings__h;
		Long lStackSize__i;
		Long lProcessID__i;
		lStackSize__i=0;
		lForm__t=K.get();
		{
			Txt t0;
			c.f.fLine=16;
			if (g->Call(ctx,(PCV[]){t0.cv()},0,684)) goto _0;
			g->Check(ctx);
			lDesiredProcessName__t=t0.get();
		}
		{
			Txt t1;
			c.f.fLine=18;
			if (g->Call(ctx,(PCV[]){t1.cv()},0,1392)) goto _0;
			g->Check(ctx);
			Bool t2;
			t2=g->CompareString(ctx,t1.get(),lDesiredProcessName__t.get())==0;
			if (!(t2.get())) goto _2;
		}
		{
			Txt t3;
			c.f.fLine=21;
			if (g->Call(ctx,(PCV[]){t3.cv(),Long(4).cv()},1,485)) goto _0;
			g->Check(ctx);
			lProjectFolder__t=t3.get();
		}
		{
			Txt t4;
			g->AddString(lProjectFolder__t.get(),KBuilds.get(),t4.get());
			g->AddString(t4.get(),K_3A.get(),lbuildFolder__t.get());
		}
		c.f.fLine=23;
		if (g->Call(ctx,(PCV[]){nullptr,lbuildFolder__t.cv(),Ref((d4_enums::optyp)3).cv()},2,475)) goto _0;
		g->Check(ctx);
		{
			Txt t6;
			g->AddString(lProjectFolder__t.get(),KSettings.get(),t6.get());
			Txt t7;
			g->AddString(t6.get(),K_3A.get(),t7.get());
			g->AddString(t7.get(),kulSCpLl7Du4.get(),lbuildSettingsPath__t.get());
		}
		{
			Long t9;
			c.f.fLine=27;
			if (g->Call(ctx,(PCV[]){t9.cv(),lbuildSettingsPath__t.cv()},1,476)) goto _0;
			g->Check(ctx);
			if (1!=t9.get()) goto _3;
		}
		goto _4;
_3:
		{
			Txt t11;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t11.cv(),Long(6).cv()},1,485)) goto _0;
			g->Check(ctx);
			g->AddString(t11.get(),kulSCpLl7Du4.get(),ltemplatePath__t.get());
		}
		c.f.fLine=30;
		if (g->Call(ctx,(PCV[]){nullptr,ltemplatePath__t.cv(),lbuildSettingsPath__t.cv()},2,541)) goto _0;
		g->Check(ctx);
_4:
		{
			Time t13;
			c.f.fLine=33;
			if (g->Call(ctx,(PCV[]){t13.cv(),lbuildSettingsPath__t.cv()},1,264)) goto _0;
			g->Check(ctx);
			lbuildsettings__h=t13.get();
		}
		if (1!=Var<Long>(ctx,vOK).get()) goto _5;
		{
			Ref t15;
			t15.setLocalRef(ctx,lbuildSettings__t.cv());
			Time t16;
			t16=lbuildsettings__h.get();
			c.f.fLine=36;
			if (g->Call(ctx,(PCV[]){nullptr,t16.cv(),t15.cv(),Long(32000).cv()},3,104)) goto _0;
			g->Check(ctx);
		}
		{
			Time t17;
			t17=lbuildsettings__h.get();
			c.f.fLine=37;
			if (g->Call(ctx,(PCV[]){nullptr,t17.cv()},1,267)) goto _0;
			g->Check(ctx);
		}
		{
			Bool t18;
			t18=Bool(0).get();
			Ref t19;
			t19.setLocalRef(ctx,lbuildSettings__t.cv());
			Txt t20;
			c.f.fLine=38;
			if (g->Call(ctx,(PCV[]){t20.cv(),t19.cv(),t18.cv()},2,720)) goto _0;
			g->Check(ctx);
			lrootNode__t=t20.get();
		}
		lelement__t=kjKDCQGtjyBY.get();
		{
			Txt t21;
			c.f.fLine=42;
			if (g->Call(ctx,(PCV[]){t21.cv(),lrootNode__t.cv(),lelement__t.cv()},2,864)) goto _0;
			g->Check(ctx);
			lelementRef__t=t21.get();
		}
		{
			Ref t22;
			t22.setLocalRef(ctx,lBuiltStructure__t.cv());
			c.f.fLine=43;
			if (g->Call(ctx,(PCV[]){nullptr,lelementRef__t.cv(),t22.cv()},2,731)) goto _0;
			g->Check(ctx);
		}
		lBuiltStructure__t=KLogging_2E4dbase.get();
		c.f.fLine=45;
		if (g->Call(ctx,(PCV[]){nullptr,lelementRef__t.cv(),lBuiltStructure__t.cv()},2,868)) goto _0;
		g->Check(ctx);
		lelement__t=kbmoXZlMq9pA.get();
		{
			Bool t23;
			c.f.fLine=49;
			if (g->Call(ctx,(PCV[]){t23.cv()},0,1572)) goto _0;
			if (!(t23.get())) goto _6;
		}
		g->AddString(lelement__t.get(),kLa2ChQdcjpU.get(),lelement__t.get());
		goto _7;
_6:
		g->AddString(lelement__t.get(),kmScCWfEpPp4.get(),lelement__t.get());
_7:
		{
			Txt t26;
			c.f.fLine=54;
			if (g->Call(ctx,(PCV[]){t26.cv(),lrootNode__t.cv(),lelement__t.cv()},2,864)) goto _0;
			g->Check(ctx);
			lelementRef__t=t26.get();
		}
		c.f.fLine=55;
		if (g->Call(ctx,(PCV[]){nullptr,lelementRef__t.cv(),lbuildFolder__t.cv()},2,868)) goto _0;
		g->Check(ctx);
		c.f.fLine=58;
		if (g->Call(ctx,(PCV[]){nullptr,lrootNode__t.cv(),lbuildSettingsPath__t.cv()},2,862)) goto _0;
		g->Check(ctx);
		c.f.fLine=59;
		if (g->Call(ctx,(PCV[]){nullptr,lrootNode__t.cv()},1,722)) goto _0;
		g->Check(ctx);
		c.f.fLine=61;
		if (g->Call(ctx,(PCV[]){nullptr,kI_UxMf2Gtu0.cv()},1,155)) goto _0;
		g->Check(ctx);
		c.f.fLine=62;
		if (g->Call(ctx,(PCV[]){nullptr,lbuildSettingsPath__t.cv()},1,871)) goto _0;
		g->Check(ctx);
		if (1!=Var<Long>(ctx,vOK).get()) goto _8;
		c.f.fLine=66;
		if (g->Call(ctx,(PCV[]){nullptr,KBuild_20Succeeded.cv()},1,41)) goto _0;
		g->Check(ctx);
		{
			Txt t28;
			g->AddString(lbuildFolder__t.get(),kT0zyqtJ3KXg.get(),t28.get());
			Txt t29;
			g->AddString(t28.get(),K_3A.get(),t29.get());
			c.f.fLine=67;
			if (g->Call(ctx,(PCV[]){nullptr,t29.cv(),Ref((d4_enums::optyp)3).cv()},2,922)) goto _0;
			g->Check(ctx);
		}
		goto _9;
_8:
		{
			Txt t30;
			c.f.fLine=69;
			if (g->Call(ctx,(PCV[]){t30.cv(),Long(7).cv()},1,485)) goto _0;
			g->Check(ctx);
			if (g->Call(ctx,(PCV[]){nullptr,t30.cv(),Ref((d4_enums::optyp)3).cv()},2,922)) goto _0;
		}
_9:
		c.f.fLine=71;
		if (g->Call(ctx,(PCV[]){nullptr,K.cv()},1,155)) goto _0;
		g->Check(ctx);
_5:
		goto _10;
_2:
		{
			Txt t31;
			c.f.fLine=80;
			if (g->Call(ctx,(PCV[]){t31.cv()},0,684)) goto _0;
			g->Check(ctx);
			Long t32;
			if (g->Call(ctx,(PCV[]){t32.cv(),t31.cv(),lStackSize__i.cv(),lDesiredProcessName__t.cv(),Ref((d4_enums::optyp)3).cv()},4,317)) goto _0;
			lProcessID__i=t32.get();
		}
		c.f.fLine=82;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,320)) goto _0;
		g->Check(ctx);
		c.f.fLine=83;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,325)) goto _0;
		g->Check(ctx);
		c.f.fLine=84;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,326)) goto _0;
		g->Check(ctx);
_10:
_0:
_1:
;
	}

}
