extern int32_t vlg;
extern Txt K_20log_2Etxt;
extern Txt KdefaultLog;
extern Txt Kenabled;
extern Txt Kfile;
extern Txt Kfolder;
extern Txt Kinitialised;
extern Txt Kk;
extern Txt Klog;
extern Txt Klogs;
extern Txt KlogsFolder;
extern Txt KmaxLogSize;
extern Txt Kname;
extern Txt Kreturn;
extern Txt Ktab;
Asm4d_Proc proc_COMPILER__LOG;
extern unsigned char D_proc_LOG__INIT[];
void proc_LOG__INIT( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG__INIT);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Obj l__4D__auto__mutex__0;
		Txt lLog__FileName__t;
		{
			Bool t0;
			c.f.fLine=22;
			if (g->Call(ctx,(PCV[]){t0.cv()},0,547)) goto _0;
			if (!(t0.get())) goto _2;
		}
_2:
		{
			Obj t1;
			c.f.fLine=27;
			if (g->Call(ctx,(PCV[]){t1.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t2;
			if (g->GetMember(ctx,t1.cv(),Klog.cv(),t2.cv())) goto _0;
			Variant t3;
			if (g->GetMember(ctx,t2.cv(),Kinitialised.cv(),t3.cv())) goto _0;
			Bool t4;
			if (g->OperationOnAny(ctx,6,t3.cv(),Value_null().cv(),t4.cv())) goto _0;
			if (!(t4.get())) goto _3;
		}
		{
			Obj t5;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t5.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t6;
			if (g->GetMember(ctx,t5.cv(),Klog.cv(),t6.cv())) goto _0;
			Bool t7;
			if (g->OperationOnAny(ctx,6,t6.cv(),Value_null().cv(),t7.cv())) goto _0;
			if (!(t7.get())) goto _4;
		}
		{
			Obj t8;
			c.f.fLine=30;
			if (g->Call(ctx,(PCV[]){t8.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Obj t9;
			if (g->Call(ctx,(PCV[]){t9.cv(),t8.cv()},1,1529)) goto _0;
			l__4D__auto__mutex__0=t9.get();
		}
		{
			Obj t10;
			c.f.fLine=31;
			if (g->Call(ctx,(PCV[]){t10.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Bool t11;
			t11=Bool(1).get();
			Obj t12;
			if (g->Call(ctx,(PCV[]){t12.cv(),Kinitialised.cv(),t11.cv(),KmaxLogSize.cv(),Long(0).cv()},4,1526)) goto _0;
			if (g->SetMember(ctx,t10.cv(),Klog.cv(),t12.cv())) goto _0;
		}
		{
			Obj t13;
			c.f.fLine=33;
			if (g->Call(ctx,(PCV[]){t13.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Obj t14;
			if (g->Call(ctx,(PCV[]){t14.cv()},0,1526)) goto _0;
			if (g->SetMember(ctx,t13.cv(),Klogs.cv(),t14.cv())) goto _0;
		}
		{
			Obj t15;
			c.f.fLine=35;
			if (g->Call(ctx,(PCV[]){t15.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Txt t16;
			if (g->Call(ctx,(PCV[]){t16.cv(),Long(9).cv()},1,90)) goto _0;
			Txt t17;
			if (g->Call(ctx,(PCV[]){t17.cv(),Long(13).cv()},1,90)) goto _0;
			Txt t18;
			if (g->Call(ctx,(PCV[]){t18.cv(),Long(10).cv()},1,90)) goto _0;
			Txt t19;
			g->AddString(t17.get(),t18.get(),t19.get());
			Txt t20;
			if (g->Call(ctx,(PCV[]){t20.cv(),Long(7).cv(),Ref((d4_enums::optyp)3).cv()},2,485)) goto _0;
			Txt t21;
			if (g->Call(ctx,(PCV[]){t21.cv(),Ref((d4_enums::optyp)3).cv()},1,489)) goto _0;
			Obj t22;
			if (g->Call(ctx,(PCV[]){t22.cv(),t21.cv()},1,1547)) goto _0;
			Variant t23;
			if (g->GetMember(ctx,t22.cv(),Kname.cv(),t23.cv())) goto _0;
			Variant t24;
			if (g->OperationOnAny(ctx,0,t23.cv(),K_20log_2Etxt.cv(),t24.cv())) goto _0;
			Obj t25;
			if (g->Call(ctx,(PCV[]){t25.cv(),Ktab.cv(),t16.cv(),Kreturn.cv(),t19.cv(),KlogsFolder.cv(),t20.cv(),KdefaultLog.cv(),t24.cv()},8,1526)) goto _0;
			if (g->SetMember(ctx,t15.cv(),Kk.cv(),t25.cv())) goto _0;
		}
		{
			Obj t26;
			l__4D__auto__mutex__0=t26.get();
		}
_4:
_3:
		{
			Variant t27;
			c.f.fLine=43;
			if (g->GetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kinitialised.cv(),t27.cv())) goto _0;
			Bool t28;
			if (g->OperationOnAny(ctx,6,t27.cv(),Value_null().cv(),t28.cv())) goto _0;
			if (!(t28.get())) goto _5;
		}
		c.f.fLine=45;
		proc_COMPILER__LOG(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Bool t29;
			t29=Bool(1).get();
			Obj t30;
			c.f.fLine=47;
			if (g->Call(ctx,(PCV[]){t30.cv(),Kinitialised.cv(),t29.cv()},2,1471)) goto _0;
			g->Check(ctx);
			Var<Obj>(ctx,vlg)=t30.get();
			Touch(ctx,vlg);
		}
		{
			Bool t31;
			t31=Bool(0).get();
			c.f.fLine=49;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kenabled.cv(),t31.cv())) goto _0;
		}
		{
			Obj t32;
			c.f.fLine=50;
			if (g->Call(ctx,(PCV[]){t32.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t33;
			if (g->GetMember(ctx,t32.cv(),Kk.cv(),t33.cv())) goto _0;
			Variant t34;
			if (g->GetMember(ctx,t33.cv(),KdefaultLog.cv(),t34.cv())) goto _0;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),t34.cv())) goto _0;
		}
		{
			Obj t35;
			c.f.fLine=51;
			if (g->Call(ctx,(PCV[]){t35.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t36;
			if (g->GetMember(ctx,t35.cv(),Kk.cv(),t36.cv())) goto _0;
			Variant t37;
			if (g->GetMember(ctx,t36.cv(),KlogsFolder.cv(),t37.cv())) goto _0;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfolder.cv(),t37.cv())) goto _0;
		}
_5:
_0:
_1:
;
	}

}
