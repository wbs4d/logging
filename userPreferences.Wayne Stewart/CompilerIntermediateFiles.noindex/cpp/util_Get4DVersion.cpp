extern Txt K;
extern Txt K0;
extern Txt K4D_20;
extern Txt K_20R;
extern Txt K_20_28;
extern Txt K_29;
extern Txt K_2E;
extern unsigned char D_proc_UTIL__GET4DVERSION[];
void proc_UTIL__GET4DVERSION( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_UTIL__GET4DVERSION);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt lTxt__release;
		Txt lversion;
		Long lbuild;
		new ( outResult) Txt();
		{
			Ref t0;
			t0.setLocalRef(ctx,lbuild.cv());
			Txt t1;
			c.f.fLine=21;
			if (g->Call(ctx,(PCV[]){t1.cv(),t0.cv()},1,493)) goto _0;
			g->Check(ctx);
			lversion=t1.get();
		}
		{
			Txt t2;
			c.f.fLine=24;
			g->GetStringChar(ctx,lversion.get(),(sLONG) 1,t2.get());
			Txt t3;
			g->GetStringChar(ctx,lversion.get(),(sLONG) 2,t3.get());
			g->AddString(t2.get(),t3.get(),Res<Txt>(outResult).get());
		}
		{
			Txt t5;
			c.f.fLine=25;
			g->GetStringChar(ctx,lversion.get(),(sLONG) 3,t5.get());
			lTxt__release=t5.get();
		}
		c.f.fLine=27;
		g->AddString(K4D_20.get(),Res<Txt>(outResult).get(),Res<Txt>(outResult).get());
		{
			Bool t7;
			t7=g->CompareString(ctx,lTxt__release.get(),K0.get())==0;
			if (!(t7.get())) goto _2;
		}
		{
			Txt t8;
			c.f.fLine=30;
			g->GetStringChar(ctx,lversion.get(),(sLONG) 4,t8.get());
			lTxt__release=t8.get();
		}
		{
			Bool t9;
			t9=g->CompareString(ctx,lTxt__release.get(),K0.get())!=0;
			Txt t10;
			g->AddString(K_2E.get(),lTxt__release.get(),t10.get());
			Bool t11;
			t11=t9.get();
			Variant t12;
			c.f.fLine=31;
			if (g->Call(ctx,(PCV[]){t12.cv(),t11.cv(),t10.cv(),K.cv()},3,955)) goto _0;
			Variant t13;
			if (g->OperationOnAny(ctx,0,Res<Txt>(outResult).cv(),t12.cv(),t13.cv())) goto _0;
			Txt t14;
			if (!g->GetValue(ctx,(PCV[]){t14.cv(),t13.cv(),nullptr})) goto _0;
			Res<Txt>(outResult)=t14.get();
		}
		goto _3;
_2:
		{
			Txt t15;
			c.f.fLine=34;
			g->AddString(Res<Txt>(outResult).get(),K_20R.get(),t15.get());
			g->AddString(t15.get(),lTxt__release.get(),Res<Txt>(outResult).get());
		}
_3:
		{
			Txt t17;
			c.f.fLine=37;
			if (g->Call(ctx,(PCV[]){t17.cv(),lbuild.cv()},1,10)) goto _0;
			Txt t18;
			g->AddString(K_20_28.get(),t17.get(),t18.get());
			Txt t19;
			g->AddString(t18.get(),K_29.get(),t19.get());
			g->AddString(Res<Txt>(outResult).get(),t19.get(),Res<Txt>(outResult).get());
		}
_0:
_1:
;
	}

}
