extern int32_t vlg;
extern Txt KdefaultLog;
extern Txt Kfile;
extern Txt Kk;
Asm4d_Proc proc_LOG__INIT;
extern unsigned char D_proc_LOG_20FILE_20NAME[];
void proc_LOG_20FILE_20NAME( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20FILE_20NAME);
	if (!ctx->doingAbort && c.f.fLine==0) {
		new ( outResult) Txt();
		c.f.fLine=22;
		proc_LOG__INIT(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Long t0;
			t0=inNbExplicitParam;
			if (1!=t0.get()) goto _2;
		}
		c.f.fLine=25;
		if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv())) goto _0;
		if (ctx->doingAbort) goto _0;
_2:
		{
			Variant t2;
			c.f.fLine=28;
			if (g->GetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),t2.cv())) goto _0;
			Txt t3;
			if (!g->GetValue(ctx,(PCV[]){t3.cv(),t2.cv(),nullptr})) goto _0;
			Long t4;
			t4=t3.get().fLength;
			Bool t5;
			t5=0==t4.get();
			if (!(t5.get())) goto _3;
		}
		{
			Obj t6;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t6.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t7;
			if (g->GetMember(ctx,t6.cv(),Kk.cv(),t7.cv())) goto _0;
			Variant t8;
			if (g->GetMember(ctx,t7.cv(),KdefaultLog.cv(),t8.cv())) goto _0;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),t8.cv())) goto _0;
		}
_3:
		{
			Variant t9;
			c.f.fLine=33;
			if (g->GetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),t9.cv())) goto _0;
			Txt t10;
			if (!g->GetValue(ctx,(PCV[]){t10.cv(),t9.cv(),nullptr})) goto _0;
			Res<Txt>(outResult)=t10.get();
		}
		goto _0;
_0:
_1:
;
	}

}
