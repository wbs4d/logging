extern int32_t vlg;
extern Txt KLOG_20THIS;
extern Txt KLog_20Writer;
extern Txt K_09;
extern Txt K_0D;
extern Txt Kenabled;
extern Txt Kfile;
extern Txt Kfolder;
extern Txt Kk;
extern Txt Kreturn;
extern Txt Ktab;
Asm4d_Proc proc_LOG_20ISO_20TIME_20STAMP;
Asm4d_Proc proc_LOG__INIT;
extern unsigned char D_proc_LOG_20ADD_20ENTRY[];
void proc_LOG_20ADD_20ENTRY( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20ADD_20ENTRY);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long v0;
		Long v1;
		Long lparameter__i;
		Txt lErrorHandler__t;
		Txt llogItem__t;
		Txt llogEntry__t;
		c.f.fLine=23;
		proc_LOG__INIT(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Variant t0;
			c.f.fLine=26;
			if (g->GetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kenabled.cv(),t0.cv())) goto _0;
			Bool t1;
			if (!g->GetValue(ctx,(PCV[]){t1.cv(),t0.cv(),nullptr})) goto _0;
			if (!(t1.get())) goto _2;
		}
		c.f.fLine=30;
		llogEntry__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		{
			Obj t2;
			c.f.fLine=31;
			if (g->Call(ctx,(PCV[]){t2.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t3;
			if (g->GetMember(ctx,t2.cv(),Kk.cv(),t3.cv())) goto _0;
			Variant t4;
			if (g->GetMember(ctx,t3.cv(),Ktab.cv(),t4.cv())) goto _0;
			Txt t5;
			if (!g->GetValue(ctx,(PCV[]){t5.cv(),t4.cv(),nullptr})) goto _0;
			Txt t6;
			if (g->Call(ctx,(PCV[]){t6.cv(),llogEntry__t.cv(),t5.cv(),K_09.cv()},3,233)) goto _0;
			llogEntry__t=t6.get();
		}
		{
			Obj t7;
			c.f.fLine=32;
			if (g->Call(ctx,(PCV[]){t7.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t8;
			if (g->GetMember(ctx,t7.cv(),Kk.cv(),t8.cv())) goto _0;
			Variant t9;
			if (g->GetMember(ctx,t8.cv(),Kreturn.cv(),t9.cv())) goto _0;
			Txt t10;
			if (!g->GetValue(ctx,(PCV[]){t10.cv(),t9.cv(),nullptr})) goto _0;
			Txt t11;
			if (g->Call(ctx,(PCV[]){t11.cv(),llogEntry__t.cv(),t10.cv(),K_0D.cv()},3,233)) goto _0;
			llogEntry__t=t11.get();
		}
		lparameter__i=2;
		{
			Long t12;
			t12=inNbExplicitParam;
			v0=t12.get();
		}
		goto _3;
_5:
		c.f.fLine=34;
		llogItem__t=Parm<Txt>(ctx,inParams,inNbParam,lparameter__i.get()).get();
		if (ctx->doingAbort) goto _0;
		{
			Obj t13;
			c.f.fLine=35;
			if (g->Call(ctx,(PCV[]){t13.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t14;
			if (g->GetMember(ctx,t13.cv(),Kk.cv(),t14.cv())) goto _0;
			Variant t15;
			if (g->GetMember(ctx,t14.cv(),Ktab.cv(),t15.cv())) goto _0;
			Txt t16;
			if (!g->GetValue(ctx,(PCV[]){t16.cv(),t15.cv(),nullptr})) goto _0;
			Txt t17;
			if (g->Call(ctx,(PCV[]){t17.cv(),llogItem__t.cv(),t16.cv(),K_09.cv()},3,233)) goto _0;
			llogItem__t=t17.get();
		}
		{
			Obj t18;
			c.f.fLine=36;
			if (g->Call(ctx,(PCV[]){t18.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t19;
			if (g->GetMember(ctx,t18.cv(),Kk.cv(),t19.cv())) goto _0;
			Variant t20;
			if (g->GetMember(ctx,t19.cv(),Kreturn.cv(),t20.cv())) goto _0;
			Txt t21;
			if (!g->GetValue(ctx,(PCV[]){t21.cv(),t20.cv(),nullptr})) goto _0;
			Txt t22;
			if (g->Call(ctx,(PCV[]){t22.cv(),llogItem__t.cv(),t21.cv(),K_0D.cv()},3,233)) goto _0;
			llogItem__t=t22.get();
		}
		{
			Obj t23;
			c.f.fLine=37;
			if (g->Call(ctx,(PCV[]){t23.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t24;
			if (g->GetMember(ctx,t23.cv(),Kk.cv(),t24.cv())) goto _0;
			Variant t25;
			if (g->GetMember(ctx,t24.cv(),Ktab.cv(),t25.cv())) goto _0;
			Variant t26;
			if (g->OperationOnAny(ctx,0,llogEntry__t.cv(),t25.cv(),t26.cv())) goto _0;
			Variant t27;
			if (g->OperationOnAny(ctx,0,t26.cv(),llogItem__t.cv(),t27.cv())) goto _0;
			Txt t28;
			if (!g->GetValue(ctx,(PCV[]){t28.cv(),t27.cv(),nullptr})) goto _0;
			llogEntry__t=t28.get();
		}
_4:
		lparameter__i=lparameter__i.get()+1;
_3:
		if (lparameter__i.get()<=v0.get()) goto _5;
_6:
		{
			Date t31;
			t31=Date(0,0,0).get();
			Time t32;
			t32=Time(0).get();
			Bool t34;
			t34=Bool(0).get();
			Time t35;
			t35=t32.get();
			Date t36;
			t36=t31.get();
			Txt t37;
			c.f.fLine=41;
			proc_LOG_20ISO_20TIME_20STAMP(glob,ctx,0,3,(PCV[]){t36.cv(),t35.cv(),t34.cv()},t37.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			Obj t38;
			if (g->Call(ctx,(PCV[]){t38.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t39;
			if (g->GetMember(ctx,t38.cv(),Kk.cv(),t39.cv())) goto _0;
			Variant t40;
			if (g->GetMember(ctx,t39.cv(),Ktab.cv(),t40.cv())) goto _0;
			Variant t41;
			if (g->OperationOnAny(ctx,0,t37.cv(),t40.cv(),t41.cv())) goto _0;
			Variant t42;
			if (g->OperationOnAny(ctx,0,t41.cv(),llogEntry__t.cv(),t42.cv())) goto _0;
			Obj t43;
			if (g->Call(ctx,(PCV[]){t43.cv()},0,1525)) goto _0;
			Variant t44;
			if (g->GetMember(ctx,t43.cv(),Kk.cv(),t44.cv())) goto _0;
			Variant t45;
			if (g->GetMember(ctx,t44.cv(),Kreturn.cv(),t45.cv())) goto _0;
			Variant t46;
			if (g->OperationOnAny(ctx,0,t42.cv(),t45.cv(),t46.cv())) goto _0;
			Txt t47;
			if (!g->GetValue(ctx,(PCV[]){t47.cv(),t46.cv(),nullptr})) goto _0;
			llogEntry__t=t47.get();
		}
		{
			Variant t48;
			c.f.fLine=43;
			if (g->GetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfolder.cv(),t48.cv())) goto _0;
			Variant t49;
			if (g->GetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),t49.cv())) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,KLog_20Writer.cv(),KLOG_20THIS.cv(),llogEntry__t.cv(),t48.cv(),t49.cv()},5,1389)) goto _0;
			g->Check(ctx);
		}
_2:
_0:
_1:
;
	}

}
