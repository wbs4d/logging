extern Txt KLOG_20BUG_20ALERT;
extern Txt KLOG_20COMPRESS;
extern Txt K_24zip_20worker;
extern Txt K_2Ezip;
extern Txt Ksuccess;
extern Txt k4o4XQhIcmFQ;
extern Txt k9zuyr1mwhZA;
extern unsigned char D_proc_LOG_20COMPRESS[];
void proc_LOG_20COMPRESS( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20COMPRESS);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long lpathType__i;
		Long lParameters__i;
		Obj ldestination__o;
		Obj lresults__o;
		Obj lSource__o;
		Txt lDesiredProcessName__t;
		Txt lArchivePath__t;
		{
			Long t0;
			t0=inNbExplicitParam;
			lParameters__i=t0.get();
		}
		lDesiredProcessName__t=K_24zip_20worker.get();
		if (0!=lParameters__i.get()) goto _3;
		c.f.fLine=34;
		if (g->Call(ctx,(PCV[]){nullptr,Long(1).cv(),KLOG_20BUG_20ALERT.cv(),KLOG_20COMPRESS.cv(),k4o4XQhIcmFQ.cv()},4,1389)) goto _0;
		g->Check(ctx);
		goto _2;
_3:
		if (1!=lParameters__i.get()) goto _4;
		c.f.fLine=37;
		Parm<Bool>(ctx,inParams,inNbParam,2)=Bool(1).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_4:
_2:
		{
			Long t3;
			c.f.fLine=44;
			t3=Parm<Txt>(ctx,inParams,inNbParam,1).get().fLength;
			if (0>=t3.get()) goto _5;
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t5;
			c.f.fLine=45;
			if (g->Call(ctx,(PCV[]){t5.cv()},0,1392)) goto _0;
			g->Check(ctx);
			Bool t6;
			t6=g->CompareString(ctx,t5.get(),lDesiredProcessName__t.get())!=0;
			if (!(t6.get())) goto _6;
		}
		{
			Bool t7;
			c.f.fLine=46;
			t7=Parm<Bool>(ctx,inParams,inNbParam,2).get();
			if (g->Call(ctx,(PCV[]){nullptr,lDesiredProcessName__t.cv(),KLOG_20COMPRESS.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),t7.cv()},4,1389)) goto _0;
			g->Check(ctx);
		}
		if (ctx->doingAbort) goto _0;
		goto _7;
_6:
		c.f.fLine=49;
		{
			Long t8;
			if (g->Call(ctx,(PCV[]){t8.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv()},1,476)) goto _0;
			g->Check(ctx);
			lpathType__i=t8.get();
		}
		if (ctx->doingAbort) goto _0;
		if (1!=lpathType__i.get()) goto _9;
		c.f.fLine=53;
		{
			Obj t10;
			if (g->Call(ctx,(PCV[]){t10.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),Long(1).cv()},2,1566)) goto _0;
			g->Check(ctx);
			lSource__o=t10.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=54;
		g->AddString(Parm<Txt>(ctx,inParams,inNbParam,1).get(),K_2Ezip.get(),lArchivePath__t.get());
		if (ctx->doingAbort) goto _0;
		goto _8;
_9:
		if (0!=lpathType__i.get()) goto _10;
		{
			Long t13;
			c.f.fLine=57;
			t13=Parm<Txt>(ctx,inParams,inNbParam,1).get().fLength;
			Long t14;
			t14=t13.get()-1;
			Txt t15;
			if (g->Call(ctx,(PCV[]){t15.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),Long(1).cv(),t14.cv()},3,12)) goto _0;
			g->AddString(t15.get(),K_2Ezip.get(),lArchivePath__t.get());
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=58;
		{
			Obj t17;
			if (g->Call(ctx,(PCV[]){t17.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),Long(1).cv()},2,1567)) goto _0;
			g->Check(ctx);
			lSource__o=t17.get();
		}
		if (ctx->doingAbort) goto _0;
		goto _8;
_10:
		{
			Txt t18;
			g->AddString(k9zuyr1mwhZA.get(),Parm<Txt>(ctx,inParams,inNbParam,1).get(),t18.get());
			c.f.fLine=61;
			if (g->Call(ctx,(PCV[]){nullptr,Long(1).cv(),KLOG_20BUG_20ALERT.cv(),KLOG_20COMPRESS.cv(),t18.cv()},4,1389)) goto _0;
			g->Check(ctx);
		}
		if (ctx->doingAbort) goto _0;
_8:
		{
			Bool t19;
			t19=!lSource__o.isNull();
			if (!(t19.get())) goto _11;
		}
		{
			Obj t20;
			c.f.fLine=67;
			if (g->Call(ctx,(PCV[]){t20.cv(),lArchivePath__t.cv(),Long(1).cv()},2,1566)) goto _0;
			g->Check(ctx);
			ldestination__o=t20.get();
		}
		{
			Obj t21;
			c.f.fLine=68;
			if (g->Call(ctx,(PCV[]){t21.cv(),lSource__o.cv(),ldestination__o.cv()},2,1640)) goto _0;
			g->Check(ctx);
			lresults__o=t21.get();
		}
		{
			Variant t22;
			c.f.fLine=70;
			if (g->GetMember(ctx,lresults__o.cv(),Ksuccess.cv(),t22.cv())) goto _0;
			Variant t23;
			if (g->OperationOnAny(ctx,8,t22.cv(),Parm<Bool>(ctx,inParams,inNbParam,2).cv(),t23.cv())) goto _0;
			Bool t24;
			if (!g->GetValue(ctx,(PCV[]){t24.cv(),t23.cv(),nullptr})) goto _0;
			if (!(t24.get())) goto _12;
		}
		if (ctx->doingAbort) goto _0;
		if (1!=lpathType__i.get()) goto _13;
		c.f.fLine=72;
		if (g->Call(ctx,(PCV[]){nullptr,Parm<Txt>(ctx,inParams,inNbParam,1).cv()},1,159)) goto _0;
		g->Check(ctx);
		if (ctx->doingAbort) goto _0;
		goto _14;
_13:
		c.f.fLine=74;
		if (g->Call(ctx,(PCV[]){nullptr,Parm<Txt>(ctx,inParams,inNbParam,1).cv(),Long(1).cv()},2,693)) goto _0;
		g->Check(ctx);
		if (ctx->doingAbort) goto _0;
_14:
_12:
_11:
_7:
_5:
_0:
_1:
;
	}

}
