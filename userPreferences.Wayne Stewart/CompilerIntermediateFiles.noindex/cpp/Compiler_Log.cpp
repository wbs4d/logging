extern int32_t vLog__DocRefs__ah;
extern int32_t vLog__Paths__at;
extern int32_t vlg;
extern Txt Kinitialised;
extern unsigned char D_proc_COMPILER__LOG[];
void proc_COMPILER__LOG( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_COMPILER__LOG);
	if (!ctx->doingAbort && c.f.fLine==0) {
		{
			Variant t0;
			c.f.fLine=19;
			if (g->GetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kinitialised.cv(),t0.cv())) goto _0;
			Bool t1;
			if (g->OperationOnAny(ctx,6,t0.cv(),Value_null().cv(),t1.cv())) goto _0;
			if (!(t1.get())) goto _2;
		}
		c.f.fLine=20;
		if (g->Call(ctx,(PCV[]){nullptr,Ref(ctx,vLog__Paths__at).cv(),Long(0).cv()},2,222)) goto _0;
		c.f.fLine=21;
		if (g->Call(ctx,(PCV[]){nullptr,Ref(ctx,vLog__DocRefs__ah).cv(),Long(0).cv()},2,1223)) goto _0;
		g->Check(ctx);
_2:
_0:
_1:
;
	}

}
