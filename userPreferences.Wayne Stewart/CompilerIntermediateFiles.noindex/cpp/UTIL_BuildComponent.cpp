extern int32_t vOK;
extern Txt K;
extern Txt KBuilds;
extern Txt KCFBundleName;
extern Txt KCancel;
extern Txt KComponents;
extern Txt KInfoPlist_2Ejson;
extern Txt KOK;
extern Txt KUTF_2D8;
extern Txt K_20;
extern Txt K_22_3B;
extern Txt K_2C_20;
extern Txt K_2C_20Mac_20to_20Basics;
extern Txt K_3A;
extern Txt K_A92004_2D;
extern Txt k4U5TfKSo$uA;
extern Txt k4_qKxzSuqlE;
extern Txt k92btG4OdU$s;
extern Txt k9gS5UdrV0QY;
extern Txt kCnQAYXvVWVM;
extern Txt kDXuhM3FS$_c;
extern Txt kGOAxKe4tGP0;
extern Txt kINGgez3jd8Q;
extern Txt kMaGss3dDlbI;
extern Txt km1Evpxhby54;
extern Txt knwtALt8HcvA;
extern Txt kwFfMvNyrN60;
Asm4d_Proc proc_LOG_20ISO_20TIME_20STAMP;
Asm4d_Proc proc_UTIL__WRITEMETHODCOMMENTS;
extern unsigned char D_proc_UTIL__BUILDCOMPONENT[];
void proc_UTIL__BUILDCOMPONENT( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_UTIL__BUILDCOMPONENT);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt lbuildSettingsPath__t;
		Txt ltemplatePath__t;
		Obj linfo__o;
		Txt lnewName__t;
		Txt lbuildFolderPath__t;
		Txt lbuiltComponent__t;
		Txt lstrings__t;
		Txt lLF;
		Txt larchive__t;
		Txt lenteredValue__t;
		Txt lyear__t;
		Txt ldate__t;
		Txt ljson__t;
		Txt lversionNumber__t;
		{
			Txt t0;
			c.f.fLine=23;
			if (g->Call(ctx,(PCV[]){t0.cv(),Long(6).cv()},1,485)) goto _0;
			g->Check(ctx);
			g->AddString(t0.get(),KInfoPlist_2Ejson.get(),ltemplatePath__t.get());
		}
		{
			Long t2;
			c.f.fLine=24;
			if (g->Call(ctx,(PCV[]){t2.cv(),ltemplatePath__t.cv()},1,476)) goto _0;
			g->Check(ctx);
			if (1!=t2.get()) goto _2;
		}
		{
			Txt t4;
			c.f.fLine=25;
			if (g->Call(ctx,(PCV[]){t4.cv(),Long(10).cv()},1,90)) goto _0;
			lLF=t4.get();
		}
		{
			Txt t5;
			c.f.fLine=26;
			if (g->Call(ctx,(PCV[]){t5.cv(),ltemplatePath__t.cv()},1,1236)) goto _0;
			g->Check(ctx);
			ljson__t=t5.get();
		}
		{
			Variant t6;
			c.f.fLine=27;
			if (g->Call(ctx,(PCV[]){t6.cv(),ljson__t.cv()},1,1218)) goto _0;
			g->Check(ctx);
			Obj t7;
			if (!g->GetValue(ctx,(PCV[]){t7.cv(),t6.cv(),nullptr})) goto _0;
			linfo__o=t7.get();
		}
		{
			Date t8;
			t8=Date(0,0,0).get();
			Time t9;
			t9=Time(0).get();
			Bool t11;
			t11=Bool(0).get();
			Time t12;
			t12=t9.get();
			Date t13;
			t13=t8.get();
			Txt t14;
			c.f.fLine=28;
			proc_LOG_20ISO_20TIME_20STAMP(glob,ctx,0,3,(PCV[]){t13.cv(),t12.cv(),t11.cv()},t14.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			ldate__t=t14.get();
		}
		{
			Txt t15;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t15.cv(),ldate__t.cv(),Long(1).cv(),Long(4).cv()},3,12)) goto _0;
			lyear__t=t15.get();
		}
		{
			Long t16;
			t16=inNbExplicitParam;
			if (1!=t16.get()) goto _3;
		}
		c.f.fLine=32;
		lversionNumber__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		Var<Long>(ctx,vOK)=1;
		Touch(ctx,vOK);
		goto _4;
_3:
		{
			Variant t18;
			c.f.fLine=35;
			if (g->GetMember(ctx,linfo__o.cv(),km1Evpxhby54.cv(),t18.cv())) goto _0;
			Txt t19;
			if (g->Call(ctx,(PCV[]){t19.cv(),kCnQAYXvVWVM.cv(),t18.cv(),KOK.cv(),KCancel.cv()},4,163)) goto _0;
			g->Check(ctx);
			lenteredValue__t=t19.get();
		}
		if (1!=Var<Long>(ctx,vOK).get()) goto _5;
		lversionNumber__t=lenteredValue__t.get();
_5:
_4:
		goto _6;
_2:
		Var<Long>(ctx,vOK)=0;
		Touch(ctx,vOK);
_6:
		if (1!=Var<Long>(ctx,vOK).get()) goto _7;
		c.f.fLine=49;
		if (g->SetMember(ctx,linfo__o.cv(),km1Evpxhby54.cv(),lversionNumber__t.cv())) goto _0;
		{
			Variant t22;
			c.f.fLine=50;
			if (g->GetMember(ctx,linfo__o.cv(),KCFBundleName.cv(),t22.cv())) goto _0;
			Variant t23;
			if (g->OperationOnAny(ctx,0,t22.cv(),K_20.cv(),t23.cv())) goto _0;
			Variant t24;
			if (g->OperationOnAny(ctx,0,t23.cv(),lversionNumber__t.cv(),t24.cv())) goto _0;
			Variant t25;
			if (g->OperationOnAny(ctx,0,t24.cv(),K_2C_20.cv(),t25.cv())) goto _0;
			Variant t26;
			if (g->OperationOnAny(ctx,0,t25.cv(),ldate__t.cv(),t26.cv())) goto _0;
			if (g->SetMember(ctx,linfo__o.cv(),kMaGss3dDlbI.cv(),t26.cv())) goto _0;
		}
		{
			Txt t27;
			g->AddString(K_A92004_2D.get(),lyear__t.get(),t27.get());
			Txt t28;
			g->AddString(t27.get(),K_2C_20Mac_20to_20Basics.get(),t28.get());
			c.f.fLine=51;
			if (g->SetMember(ctx,linfo__o.cv(),k92btG4OdU$s.cv(),t28.cv())) goto _0;
		}
		{
			Txt t29;
			c.f.fLine=53;
			if (g->Call(ctx,(PCV[]){t29.cv(),linfo__o.cv(),Ref((d4_enums::optyp)3).cv()},2,1217)) goto _0;
			g->Check(ctx);
			ljson__t=t29.get();
		}
		c.f.fLine=54;
		if (g->Call(ctx,(PCV[]){nullptr,ltemplatePath__t.cv()},1,159)) goto _0;
		g->Check(ctx);
		c.f.fLine=55;
		if (g->Call(ctx,(PCV[]){nullptr,ltemplatePath__t.cv(),ljson__t.cv(),KUTF_2D8.cv(),Long(4).cv()},4,1237)) goto _0;
		g->Check(ctx);
		{
			Txt t30;
			g->AddString(kwFfMvNyrN60.get(),lLF.get(),t30.get());
			g->AddString(t30.get(),lLF.get(),lstrings__t.get());
		}
		{
			Txt t32;
			g->AddString(lstrings__t.get(),k4U5TfKSo$uA.get(),t32.get());
			Variant t33;
			c.f.fLine=59;
			if (g->GetMember(ctx,linfo__o.cv(),KCFBundleName.cv(),t33.cv())) goto _0;
			Variant t34;
			if (g->OperationOnAny(ctx,0,t32.cv(),t33.cv(),t34.cv())) goto _0;
			Variant t35;
			if (g->OperationOnAny(ctx,0,t34.cv(),K_22_3B.cv(),t35.cv())) goto _0;
			Variant t36;
			if (g->OperationOnAny(ctx,0,t35.cv(),lLF.cv(),t36.cv())) goto _0;
			Txt t37;
			if (!g->GetValue(ctx,(PCV[]){t37.cv(),t36.cv(),nullptr})) goto _0;
			lstrings__t=t37.get();
		}
		{
			Txt t38;
			g->AddString(lstrings__t.get(),kINGgez3jd8Q.get(),t38.get());
			Variant t39;
			c.f.fLine=60;
			if (g->GetMember(ctx,linfo__o.cv(),km1Evpxhby54.cv(),t39.cv())) goto _0;
			Variant t40;
			if (g->OperationOnAny(ctx,0,t38.cv(),t39.cv(),t40.cv())) goto _0;
			Variant t41;
			if (g->OperationOnAny(ctx,0,t40.cv(),K_22_3B.cv(),t41.cv())) goto _0;
			Variant t42;
			if (g->OperationOnAny(ctx,0,t41.cv(),lLF.cv(),t42.cv())) goto _0;
			Txt t43;
			if (!g->GetValue(ctx,(PCV[]){t43.cv(),t42.cv(),nullptr})) goto _0;
			lstrings__t=t43.get();
		}
		{
			Txt t44;
			g->AddString(lstrings__t.get(),knwtALt8HcvA.get(),t44.get());
			Variant t45;
			c.f.fLine=61;
			if (g->GetMember(ctx,linfo__o.cv(),kMaGss3dDlbI.cv(),t45.cv())) goto _0;
			Variant t46;
			if (g->OperationOnAny(ctx,0,t44.cv(),t45.cv(),t46.cv())) goto _0;
			Variant t47;
			if (g->OperationOnAny(ctx,0,t46.cv(),K_22_3B.cv(),t47.cv())) goto _0;
			Variant t48;
			if (g->OperationOnAny(ctx,0,t47.cv(),lLF.cv(),t48.cv())) goto _0;
			Txt t49;
			if (!g->GetValue(ctx,(PCV[]){t49.cv(),t48.cv(),nullptr})) goto _0;
			lstrings__t=t49.get();
		}
		{
			Txt t50;
			g->AddString(lstrings__t.get(),k4_qKxzSuqlE.get(),t50.get());
			Variant t51;
			c.f.fLine=62;
			if (g->GetMember(ctx,linfo__o.cv(),k92btG4OdU$s.cv(),t51.cv())) goto _0;
			Variant t52;
			if (g->OperationOnAny(ctx,0,t50.cv(),t51.cv(),t52.cv())) goto _0;
			Variant t53;
			if (g->OperationOnAny(ctx,0,t52.cv(),K_22_3B.cv(),t53.cv())) goto _0;
			Txt t54;
			if (!g->GetValue(ctx,(PCV[]){t54.cv(),t53.cv(),nullptr})) goto _0;
			lstrings__t=t54.get();
		}
		{
			Txt t55;
			c.f.fLine=64;
			if (g->Call(ctx,(PCV[]){t55.cv(),Long(6).cv()},1,485)) goto _0;
			g->Check(ctx);
			g->AddString(t55.get(),k9gS5UdrV0QY.get(),ltemplatePath__t.get());
		}
		c.f.fLine=66;
		if (g->Call(ctx,(PCV[]){nullptr,ltemplatePath__t.cv()},1,159)) goto _0;
		g->Check(ctx);
		c.f.fLine=67;
		if (g->Call(ctx,(PCV[]){nullptr,ltemplatePath__t.cv(),lstrings__t.cv(),KUTF_2D8.cv(),Long(4).cv()},4,1237)) goto _0;
		g->Check(ctx);
_7:
		if (1!=Var<Long>(ctx,vOK).get()) goto _8;
		{
			Txt t58;
			c.f.fLine=72;
			if (g->Call(ctx,(PCV[]){t58.cv(),Long(20).cv()},1,1418)) goto _0;
			g->Check(ctx);
			lbuildSettingsPath__t=t58.get();
		}
		{
			Txt t59;
			c.f.fLine=73;
			if (g->Call(ctx,(PCV[]){t59.cv(),Long(4).cv()},1,485)) goto _0;
			g->Check(ctx);
			Txt t60;
			g->AddString(t59.get(),KBuilds.get(),t60.get());
			Txt t61;
			g->AddString(t60.get(),K_3A.get(),t61.get());
			Txt t62;
			g->AddString(t61.get(),KComponents.get(),t62.get());
			g->AddString(t62.get(),K_3A.get(),lbuildFolderPath__t.get());
		}
		{
			Bool t64;
			t64=Bool(1).get();
			Bool t65;
			t65=Bool(1).get();
			Txt t66;
			t66=K.get();
			c.f.fLine=75;
			proc_UTIL__WRITEMETHODCOMMENTS(glob,ctx,3,3,(PCV[]){t66.cv(),t65.cv(),t64.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		c.f.fLine=76;
		if (g->Call(ctx,(PCV[]){nullptr,lbuildSettingsPath__t.cv()},1,871)) goto _0;
		g->Check(ctx);
		{
			Bool t67;
			t67=Bool(0).get();
			Bool t68;
			t68=Bool(1).get();
			Txt t69;
			t69=K.get();
			c.f.fLine=77;
			proc_UTIL__WRITEMETHODCOMMENTS(glob,ctx,3,3,(PCV[]){t69.cv(),t68.cv(),t67.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		if (1!=Var<Long>(ctx,vOK).get()) goto _9;
		c.f.fLine=80;
		if (g->Call(ctx,(PCV[]){nullptr,lbuildFolderPath__t.cv(),Ref((d4_enums::optyp)3).cv()},2,922)) goto _0;
		g->Check(ctx);
		c.f.fLine=81;
		if (g->Call(ctx,(PCV[]){nullptr,kDXuhM3FS$_c.cv()},1,41)) goto _0;
		g->Check(ctx);
		goto _10;
_9:
		c.f.fLine=83;
		if (g->Call(ctx,(PCV[]){nullptr,kGOAxKe4tGP0.cv()},1,41)) goto _0;
		g->Check(ctx);
_10:
_8:
_0:
_1:
;
	}

}
