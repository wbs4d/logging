extern Txt KDescription_3A_20;
extern Txt KLine_3A_20;
extern Txt KLog__Alert2;
extern Txt KdisplayAlert;
extern Txt KerrorDetails;
extern Txt KlineNumber;
extern Txt KmethodName;
extern Txt k4vCqL7oYN9c;
extern unsigned char D_proc_LOG_20BUG_20ALERT[];
void proc_LOG_20BUG_20ALERT( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20BUG_20ALERT);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt llineNumber__t;
		Txt lerrorMethod__t;
		Bool lisPreemptive;
		Txt ltab;
		Txt lCR;
		Txt lAlert__t;
		{
			Txt t0;
			c.f.fLine=47;
			if (g->Call(ctx,(PCV[]){t0.cv(),Long(13).cv()},1,90)) goto _0;
			lCR=t0.get();
		}
		{
			Txt t1;
			c.f.fLine=48;
			if (g->Call(ctx,(PCV[]){t1.cv(),Long(9).cv()},1,90)) goto _0;
			ltab=t1.get();
		}
		{
			Long t2;
			t2=inNbExplicitParam;
			if (4!=t2.get()) goto _3;
		}
		c.f.fLine=52;
		{
			Txt t4;
			if (g->Call(ctx,(PCV[]){t4.cv(),Parm<Long>(ctx,inParams,inNbParam,3).cv()},1,10)) goto _0;
			llineNumber__t=t4.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t5;
			c.f.fLine=53;
			if (!g->GetValue(ctx,(PCV[]){t5.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			lerrorMethod__t=t5.get();
		}
		if (ctx->doingAbort) goto _0;
		goto _2;
_3:
		{
			Long t6;
			t6=inNbExplicitParam;
			if (3!=t6.get()) goto _4;
		}
		c.f.fLine=56;
		{
			Txt t8;
			if (g->Call(ctx,(PCV[]){t8.cv(),Parm<Long>(ctx,inParams,inNbParam,3).cv()},1,10)) goto _0;
			llineNumber__t=t8.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t9;
			c.f.fLine=57;
			if (!g->GetValue(ctx,(PCV[]){t9.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			lerrorMethod__t=t9.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=58;
		Parm<Bool>(ctx,inParams,inNbParam,4)=Bool(1).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_4:
		{
			Long t10;
			t10=inNbExplicitParam;
			if (2!=t10.get()) goto _5;
		}
		{
			Txt t12;
			c.f.fLine=61;
			if (!g->GetValue(ctx,(PCV[]){t12.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			lerrorMethod__t=t12.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=62;
		Parm<Bool>(ctx,inParams,inNbParam,4)=Bool(1).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_5:
		{
			Variant t13;
			c.f.fLine=64;
			if (!g->GetValue(ctx,(PCV[]){t13.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			Long t14;
			if (g->Call(ctx,(PCV[]){t14.cv(),t13.cv()},1,1509)) goto _0;
			Bool t15;
			t15=2==t14.get();
			if (!(t15.get())) goto _6;
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t16;
			c.f.fLine=65;
			if (!g->GetValue(ctx,(PCV[]){t16.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			lerrorMethod__t=t16.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=66;
		Parm<Bool>(ctx,inParams,inNbParam,4)=Bool(1).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_6:
		{
			Variant t17;
			c.f.fLine=68;
			if (!g->GetValue(ctx,(PCV[]){t17.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			Long t18;
			if (g->Call(ctx,(PCV[]){t18.cv(),t17.cv()},1,1509)) goto _0;
			Bool t19;
			t19=38==t18.get();
			if (!(t19.get())) goto _7;
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t20;
			c.f.fLine=69;
			if (!g->GetValue(ctx,(PCV[]){t20.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			Variant t21;
			if (g->Call(ctx,(PCV[]){t21.cv(),t20.cv(),KmethodName.cv()},2,1496)) goto _0;
			g->Check(ctx);
			Txt t22;
			if (!g->GetValue(ctx,(PCV[]){t22.cv(),t21.cv(),nullptr})) goto _0;
			lerrorMethod__t=t22.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t23;
			c.f.fLine=70;
			if (!g->GetValue(ctx,(PCV[]){t23.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			Variant t24;
			if (g->Call(ctx,(PCV[]){t24.cv(),t23.cv(),KerrorDetails.cv()},2,1496)) goto _0;
			g->Check(ctx);
			Txt t25;
			if (!g->GetValue(ctx,(PCV[]){t25.cv(),t24.cv(),nullptr})) goto _0;
			Parm<Txt>(ctx,inParams,inNbParam,2)=t25.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t26;
			c.f.fLine=71;
			if (!g->GetValue(ctx,(PCV[]){t26.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			Variant t27;
			if (g->Call(ctx,(PCV[]){t27.cv(),t26.cv(),KlineNumber.cv()},2,1496)) goto _0;
			g->Check(ctx);
			Bool t28;
			if (g->OperationOnAny(ctx,7,t27.cv(),Value_null().cv(),t28.cv())) goto _0;
			if (!(t28.get())) goto _8;
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t29;
			c.f.fLine=72;
			if (!g->GetValue(ctx,(PCV[]){t29.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			Variant t30;
			if (g->Call(ctx,(PCV[]){t30.cv(),t29.cv(),KlineNumber.cv()},2,1496)) goto _0;
			g->Check(ctx);
			Txt t31;
			if (g->Call(ctx,(PCV[]){t31.cv(),t30.cv()},1,10)) goto _0;
			llineNumber__t=t31.get();
		}
		if (ctx->doingAbort) goto _0;
_8:
		{
			Variant t32;
			c.f.fLine=74;
			if (!g->GetValue(ctx,(PCV[]){t32.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			Variant t33;
			if (g->Call(ctx,(PCV[]){t33.cv(),t32.cv(),KdisplayAlert.cv()},2,1496)) goto _0;
			g->Check(ctx);
			Bool t34;
			if (g->OperationOnAny(ctx,7,t33.cv(),Value_null().cv(),t34.cv())) goto _0;
			if (!(t34.get())) goto _9;
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t35;
			c.f.fLine=75;
			if (!g->GetValue(ctx,(PCV[]){t35.cv(),Parm<Variant>(ctx,inParams,inNbParam,1).cv(),nullptr})) goto _0;
			Variant t36;
			if (g->Call(ctx,(PCV[]){t36.cv(),t35.cv(),KdisplayAlert.cv()},2,1496)) goto _0;
			g->Check(ctx);
			Txt t37;
			if (!g->GetValue(ctx,(PCV[]){t37.cv(),t36.cv(),nullptr})) goto _0;
			llineNumber__t=t37.get();
		}
		if (ctx->doingAbort) goto _0;
		goto _10;
_9:
		c.f.fLine=77;
		Parm<Bool>(ctx,inParams,inNbParam,4)=Bool(1).get();
		if (ctx->doingAbort) goto _0;
_10:
		goto _2;
_7:
_2:
		g->AddString(k4vCqL7oYN9c.get(),lerrorMethod__t.get(),lAlert__t.get());
		{
			Long t39;
			t39=llineNumber__t.get().fLength;
			if (0==t39.get()) goto _11;
		}
		{
			Txt t41;
			g->AddString(lAlert__t.get(),lCR.get(),t41.get());
			Txt t42;
			g->AddString(t41.get(),KLine_3A_20.get(),t42.get());
			g->AddString(t42.get(),llineNumber__t.get(),lAlert__t.get());
		}
_11:
		{
			Long t44;
			c.f.fLine=88;
			t44=Parm<Txt>(ctx,inParams,inNbParam,2).get().fLength;
			if (0==t44.get()) goto _12;
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t46;
			g->AddString(lAlert__t.get(),lCR.get(),t46.get());
			Txt t47;
			g->AddString(t46.get(),KDescription_3A_20.get(),t47.get());
			g->AddString(t47.get(),Parm<Txt>(ctx,inParams,inNbParam,2).get(),lAlert__t.get());
		}
		if (ctx->doingAbort) goto _0;
_12:
		if (!(Parm<Bool>(ctx,inParams,inNbParam,4).get())) goto _13;
		if (ctx->doingAbort) goto _0;
		c.f.fLine=94;
		if (g->Call(ctx,(PCV[]){nullptr,Long(1).cv(),KLog__Alert2.cv(),lAlert__t.cv()},3,1389)) goto _0;
		g->Check(ctx);
_13:
		{
			Txt t49;
			c.f.fLine=98;
			if (g->Call(ctx,(PCV[]){t49.cv(),lAlert__t.cv(),lCR.cv(),ltab.cv()},3,233)) goto _0;
			lAlert__t=t49.get();
		}
		c.f.fLine=100;
		if (g->Call(ctx,(PCV[]){nullptr,Long(3).cv(),lAlert__t.cv()},2,667)) goto _0;
		g->Check(ctx);
		c.f.fLine=101;
		if (g->Call(ctx,(PCV[]){nullptr,Long(1).cv(),lAlert__t.cv()},2,667)) goto _0;
		g->Check(ctx);
_0:
_1:
;
	}

}
