extern Txt KOn_20Activate;
extern Txt KOn_20After_20Edit;
extern Txt KOn_20After_20Sort;
extern Txt KOn_20Clicked;
extern Txt KOn_20Close_20Box;
extern Txt KOn_20Close_20Detail;
extern Txt KOn_20Collapse;
extern Txt KOn_20Column_20Moved;
extern Txt KOn_20Deactivate;
extern Txt KOn_20Drag_20Over;
extern Txt KOn_20Drop;
extern Txt KOn_20Expand;
extern Txt KOn_20Footer_20Click;
extern Txt KOn_20Header;
extern Txt KOn_20Header_20Click;
extern Txt KOn_20Load;
extern Txt KOn_20Load_20Record;
extern Txt KOn_20Long_20Click;
extern Txt KOn_20Losing_20Focus;
extern Txt KOn_20Mouse_20Enter;
extern Txt KOn_20Mouse_20Leave;
extern Txt KOn_20Mouse_20Move;
extern Txt KOn_20Mouse_20Up;
extern Txt KOn_20Open_20Detail;
extern Txt KOn_20Outside_20Call;
extern Txt KOn_20Page_20Change;
extern Txt KOn_20Plug_20in_20Area;
extern Txt KOn_20Resize;
extern Txt KOn_20Row_20Moved;
extern Txt KOn_20Row_20Resize;
extern Txt KOn_20Scroll;
extern Txt KOn_20Timer;
extern Txt KOn_20Unload;
extern Txt KOn_20VP_20Ready;
extern Txt KOn_20Validate;
extern Txt k1DHmFETHxWQ;
extern Txt kEJsHI4NZKW0;
extern Txt kFEsefpZRclY;
extern Txt kFfsYdNQWc5Q;
extern Txt kFsLD90TMInY;
extern Txt kHocRRibzYi4;
extern Txt kLa$MMHhYAec;
extern Txt kLx7d685Y0gM;
extern Txt kU54LpXosxfI;
extern Txt kV45DI_notQI;
extern Txt kYys9zdgdPAM;
extern Txt kZW_cpt0v_JM;
extern Txt k__0Z5cl3fzk;
extern Txt kbRUtJxWCf3k;
extern Txt keT5PoUuRdKk;
extern Txt kgYl2rL3a04Q;
extern Txt kiT3tRAZbmO4;
extern Txt knQqu$C0MhEE;
extern Txt kpEu2I60r$MQ;
extern Txt ksFTDZFQsE4E;
extern Txt ktZFuUAhH6WU;
extern Txt kv1M$1NHPc0A;
extern Txt ky$c3LHq6Hfc;
extern Txt kyOrzE60EC4M;
extern unsigned char D_proc_LOG_20FORM_20EVENT[];
void proc_LOG_20FORM_20EVENT( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20FORM_20EVENT);
	if (!ctx->doingAbort) {
		Long lformEventCode__i;
		new ( outResult) Txt();
		{
			Long t0;
			c.f.fLine=20;
			if (g->Call(ctx,(PCV[]){t0.cv()},0,388)) goto _0;
			g->Check(ctx);
			lformEventCode__i=t0.get();
		}
		if (1!=lformEventCode__i.get()) goto _3;
		c.f.fLine=25;
		Res<Txt>(outResult)=KOn_20Load.get();
		goto _2;
_3:
		if (2!=lformEventCode__i.get()) goto _4;
		c.f.fLine=28;
		Res<Txt>(outResult)=KOn_20Mouse_20Up.get();
		goto _2;
_4:
		if (3!=lformEventCode__i.get()) goto _5;
		c.f.fLine=31;
		Res<Txt>(outResult)=KOn_20Validate.get();
		goto _2;
_5:
		if (4!=lformEventCode__i.get()) goto _6;
		c.f.fLine=34;
		Res<Txt>(outResult)=KOn_20Clicked.get();
		goto _2;
_6:
		if (5!=lformEventCode__i.get()) goto _7;
		c.f.fLine=37;
		Res<Txt>(outResult)=KOn_20Header.get();
		goto _2;
_7:
		if (6!=lformEventCode__i.get()) goto _8;
		c.f.fLine=40;
		Res<Txt>(outResult)=kiT3tRAZbmO4.get();
		goto _2;
_8:
		if (7!=lformEventCode__i.get()) goto _9;
		c.f.fLine=43;
		Res<Txt>(outResult)=k1DHmFETHxWQ.get();
		goto _2;
_9:
		if (8!=lformEventCode__i.get()) goto _10;
		c.f.fLine=46;
		Res<Txt>(outResult)=kFsLD90TMInY.get();
		goto _2;
_10:
		if (9!=lformEventCode__i.get()) goto _11;
		c.f.fLine=49;
		Res<Txt>(outResult)=KOn_20VP_20Ready.get();
		goto _2;
_11:
		if (10!=lformEventCode__i.get()) goto _12;
		c.f.fLine=52;
		Res<Txt>(outResult)=KOn_20Outside_20Call.get();
		goto _2;
_12:
		if (11!=lformEventCode__i.get()) goto _13;
		c.f.fLine=55;
		Res<Txt>(outResult)=KOn_20Activate.get();
		goto _2;
_13:
		if (12!=lformEventCode__i.get()) goto _14;
		c.f.fLine=58;
		Res<Txt>(outResult)=KOn_20Deactivate.get();
		goto _2;
_14:
		if (13!=lformEventCode__i.get()) goto _15;
		c.f.fLine=61;
		Res<Txt>(outResult)=kbRUtJxWCf3k.get();
		goto _2;
_15:
		if (14!=lformEventCode__i.get()) goto _16;
		c.f.fLine=64;
		Res<Txt>(outResult)=KOn_20Losing_20Focus.get();
		goto _2;
_16:
		if (15!=lformEventCode__i.get()) goto _17;
		c.f.fLine=67;
		Res<Txt>(outResult)=kgYl2rL3a04Q.get();
		goto _2;
_17:
		if (16!=lformEventCode__i.get()) goto _18;
		c.f.fLine=70;
		Res<Txt>(outResult)=KOn_20Drop.get();
		goto _2;
_18:
		if (17!=lformEventCode__i.get()) goto _19;
		c.f.fLine=73;
		Res<Txt>(outResult)=kU54LpXosxfI.get();
		goto _2;
_19:
		if (18!=lformEventCode__i.get()) goto _20;
		c.f.fLine=76;
		Res<Txt>(outResult)=kHocRRibzYi4.get();
		goto _2;
_20:
		if (19!=lformEventCode__i.get()) goto _21;
		c.f.fLine=79;
		Res<Txt>(outResult)=KOn_20Plug_20in_20Area.get();
		goto _2;
_21:
		if (21!=lformEventCode__i.get()) goto _22;
		c.f.fLine=82;
		Res<Txt>(outResult)=KOn_20Drag_20Over.get();
		goto _2;
_22:
		if (22!=lformEventCode__i.get()) goto _23;
		c.f.fLine=85;
		Res<Txt>(outResult)=KOn_20Close_20Box.get();
		goto _2;
_23:
		if (23!=lformEventCode__i.get()) goto _24;
		c.f.fLine=88;
		Res<Txt>(outResult)=kEJsHI4NZKW0.get();
		goto _2;
_24:
		if (24!=lformEventCode__i.get()) goto _25;
		c.f.fLine=91;
		Res<Txt>(outResult)=KOn_20Unload.get();
		goto _2;
_25:
		if (25!=lformEventCode__i.get()) goto _26;
		c.f.fLine=94;
		Res<Txt>(outResult)=KOn_20Open_20Detail.get();
		goto _2;
_26:
		if (26!=lformEventCode__i.get()) goto _27;
		c.f.fLine=97;
		Res<Txt>(outResult)=KOn_20Close_20Detail.get();
		goto _2;
_27:
		if (27!=lformEventCode__i.get()) goto _28;
		c.f.fLine=100;
		Res<Txt>(outResult)=KOn_20Timer.get();
		goto _2;
_28:
		if (28!=lformEventCode__i.get()) goto _29;
		c.f.fLine=103;
		Res<Txt>(outResult)=kFfsYdNQWc5Q.get();
		goto _2;
_29:
		if (29!=lformEventCode__i.get()) goto _30;
		c.f.fLine=106;
		Res<Txt>(outResult)=KOn_20Resize.get();
		goto _2;
_30:
		if (30!=lformEventCode__i.get()) goto _31;
		c.f.fLine=109;
		Res<Txt>(outResult)=KOn_20After_20Sort.get();
		goto _2;
_31:
		if (31!=lformEventCode__i.get()) goto _32;
		c.f.fLine=112;
		Res<Txt>(outResult)=kZW_cpt0v_JM.get();
		goto _2;
_32:
		if (32!=lformEventCode__i.get()) goto _33;
		c.f.fLine=115;
		Res<Txt>(outResult)=KOn_20Column_20Moved.get();
		goto _2;
_33:
		if (33!=lformEventCode__i.get()) goto _34;
		c.f.fLine=118;
		Res<Txt>(outResult)=kV45DI_notQI.get();
		goto _2;
_34:
		if (34!=lformEventCode__i.get()) goto _35;
		c.f.fLine=121;
		Res<Txt>(outResult)=KOn_20Row_20Moved.get();
		goto _2;
_35:
		if (35!=lformEventCode__i.get()) goto _36;
		c.f.fLine=124;
		Res<Txt>(outResult)=KOn_20Mouse_20Enter.get();
		goto _2;
_36:
		if (36!=lformEventCode__i.get()) goto _37;
		c.f.fLine=127;
		Res<Txt>(outResult)=KOn_20Mouse_20Leave.get();
		goto _2;
_37:
		if (37!=lformEventCode__i.get()) goto _38;
		c.f.fLine=130;
		Res<Txt>(outResult)=KOn_20Mouse_20Move.get();
		goto _2;
_38:
		if (38!=lformEventCode__i.get()) goto _39;
		c.f.fLine=133;
		Res<Txt>(outResult)=kv1M$1NHPc0A.get();
		goto _2;
_39:
		if (39!=lformEventCode__i.get()) goto _40;
		c.f.fLine=136;
		Res<Txt>(outResult)=KOn_20Long_20Click.get();
		goto _2;
_40:
		if (40!=lformEventCode__i.get()) goto _41;
		c.f.fLine=139;
		Res<Txt>(outResult)=KOn_20Load_20Record.get();
		goto _2;
_41:
		if (41!=lformEventCode__i.get()) goto _42;
		c.f.fLine=142;
		Res<Txt>(outResult)=kLx7d685Y0gM.get();
		goto _2;
_42:
		if (42!=lformEventCode__i.get()) goto _43;
		c.f.fLine=145;
		Res<Txt>(outResult)=KOn_20Header_20Click.get();
		goto _2;
_43:
		if (43!=lformEventCode__i.get()) goto _44;
		c.f.fLine=148;
		Res<Txt>(outResult)=KOn_20Expand.get();
		goto _2;
_44:
		if (44!=lformEventCode__i.get()) goto _45;
		c.f.fLine=151;
		Res<Txt>(outResult)=KOn_20Collapse.get();
		goto _2;
_45:
		if (45!=lformEventCode__i.get()) goto _46;
		c.f.fLine=154;
		Res<Txt>(outResult)=KOn_20After_20Edit.get();
		goto _2;
_46:
		if (46!=lformEventCode__i.get()) goto _47;
		c.f.fLine=157;
		Res<Txt>(outResult)=kYys9zdgdPAM.get();
		goto _2;
_47:
		if (47!=lformEventCode__i.get()) goto _48;
		c.f.fLine=160;
		Res<Txt>(outResult)=kyOrzE60EC4M.get();
		goto _2;
_48:
		if (48!=lformEventCode__i.get()) goto _49;
		c.f.fLine=163;
		Res<Txt>(outResult)=knQqu$C0MhEE.get();
		goto _2;
_49:
		if (49!=lformEventCode__i.get()) goto _50;
		c.f.fLine=166;
		Res<Txt>(outResult)=kFEsefpZRclY.get();
		goto _2;
_50:
		if (50!=lformEventCode__i.get()) goto _51;
		c.f.fLine=169;
		Res<Txt>(outResult)=keT5PoUuRdKk.get();
		goto _2;
_51:
		if (51!=lformEventCode__i.get()) goto _52;
		c.f.fLine=172;
		Res<Txt>(outResult)=kpEu2I60r$MQ.get();
		goto _2;
_52:
		if (52!=lformEventCode__i.get()) goto _53;
		c.f.fLine=175;
		Res<Txt>(outResult)=ktZFuUAhH6WU.get();
		goto _2;
_53:
		if (53!=lformEventCode__i.get()) goto _54;
		c.f.fLine=178;
		Res<Txt>(outResult)=ksFTDZFQsE4E.get();
		goto _2;
_54:
		if (54!=lformEventCode__i.get()) goto _55;
		c.f.fLine=181;
		Res<Txt>(outResult)=ky$c3LHq6Hfc.get();
		goto _2;
_55:
		if (56!=lformEventCode__i.get()) goto _56;
		c.f.fLine=184;
		Res<Txt>(outResult)=KOn_20Page_20Change.get();
		goto _2;
_56:
		if (57!=lformEventCode__i.get()) goto _57;
		c.f.fLine=187;
		Res<Txt>(outResult)=KOn_20Footer_20Click.get();
		goto _2;
_57:
		if (58!=lformEventCode__i.get()) goto _58;
		c.f.fLine=190;
		Res<Txt>(outResult)=kLa$MMHhYAec.get();
		goto _2;
_58:
		if (59!=lformEventCode__i.get()) goto _59;
		c.f.fLine=193;
		Res<Txt>(outResult)=KOn_20Scroll.get();
		goto _2;
_59:
		if (60!=lformEventCode__i.get()) goto _60;
		c.f.fLine=196;
		Res<Txt>(outResult)=KOn_20Row_20Resize.get();
		goto _2;
_60:
		{
			Txt t59;
			c.f.fLine=200;
			if (g->Call(ctx,(PCV[]){t59.cv(),lformEventCode__i.cv()},1,10)) goto _0;
			g->AddString(k__0Z5cl3fzk.get(),t59.get(),Res<Txt>(outResult).get());
		}
_2:
_0:
_1:
;
	}

}
