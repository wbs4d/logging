extern int32_t vlg;
extern Txt Kenabled;
Asm4d_Proc proc_LOG__INIT;
extern unsigned char D_proc_LOG_20ENABLE[];
void proc_LOG_20ENABLE( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20ENABLE);
	if (!ctx->doingAbort) {
		new ( outResult) Bool();
		c.f.fLine=21;
		proc_LOG__INIT(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Long t0;
			t0=inNbExplicitParam;
			if (1>t0.get()) goto _2;
		}
		{
			Bool t2;
			c.f.fLine=24;
			t2=Parm<Bool>(ctx,inParams,inNbParam,1).get();
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kenabled.cv(),t2.cv())) goto _0;
		}
		if (ctx->doingAbort) goto _0;
_2:
		{
			Variant t3;
			c.f.fLine=27;
			if (g->GetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kenabled.cv(),t3.cv())) goto _0;
			Bool t4;
			if (!g->GetValue(ctx,(PCV[]){t4.cv(),t3.cv(),nullptr})) goto _0;
			Res<Bool>(outResult)=t4.get();
		}
		goto _0;
_0:
_1:
;
	}

}
