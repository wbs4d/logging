extern int32_t vLog__DocRefs__ah;
extern int32_t vLog__Paths__at;
extern Txt KfileName;
extern Txt KfolderPath;
extern Txt Klogs;
Asm4d_Proc proc_LOG__INIT;
extern unsigned char D_proc_LOG_20CLOSE_20LOG2[];
void proc_LOG_20CLOSE_20LOG2( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20CLOSE_20LOG2);
	if (!ctx->doingAbort) {
		Long v0;
		Long v1;
		Txt lPathToLogFile__t;
		Long llog__i;
		Time lDocRef__h;
		Obj lTemp__o;
		c.f.fLine=18;
		proc_LOG__INIT(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Long t0;
			t0=inNbExplicitParam;
			if (1!=t0.get()) goto _2;
		}
		{
			Long t2;
			c.f.fLine=27;
			t2=Parm<Txt>(ctx,inParams,inNbParam,1).get().fLength;
			if (0>=t2.get()) goto _3;
		}
		if (ctx->doingAbort) goto _0;
		{
			Obj t4;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t4.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t5;
			if (g->GetMember(ctx,t4.cv(),Klogs.cv(),t5.cv())) goto _0;
			Variant t6;
			if (g->GetMember(ctx,t5.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),t6.cv())) goto _0;
			Bool t7;
			if (g->OperationOnAny(ctx,7,t6.cv(),Value_null().cv(),t7.cv())) goto _0;
			if (!(t7.get())) goto _4;
		}
		if (ctx->doingAbort) goto _0;
		{
			Obj t8;
			c.f.fLine=30;
			if (g->Call(ctx,(PCV[]){t8.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t9;
			if (g->GetMember(ctx,t8.cv(),Klogs.cv(),t9.cv())) goto _0;
			Variant t10;
			if (g->GetMember(ctx,t9.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),t10.cv())) goto _0;
			Obj t11;
			if (!g->GetValue(ctx,(PCV[]){t11.cv(),t10.cv(),nullptr})) goto _0;
			lTemp__o=t11.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t12;
			c.f.fLine=32;
			if (g->GetMember(ctx,lTemp__o.cv(),KfolderPath.cv(),t12.cv())) goto _0;
			Variant t13;
			if (g->GetMember(ctx,lTemp__o.cv(),KfileName.cv(),t13.cv())) goto _0;
			Variant t14;
			if (g->OperationOnAny(ctx,0,t12.cv(),t13.cv(),t14.cv())) goto _0;
			Txt t15;
			if (!g->GetValue(ctx,(PCV[]){t15.cv(),t14.cv(),nullptr})) goto _0;
			lPathToLogFile__t=t15.get();
		}
		{
			Long t16;
			c.f.fLine=34;
			if (g->Call(ctx,(PCV[]){t16.cv(),Ref(ctx,vLog__Paths__at).cv(),lPathToLogFile__t.cv()},2,230)) goto _0;
			llog__i=t16.get();
		}
		if (0>=llog__i.get()) goto _5;
		c.f.fLine=37;
		lDocRef__h=Var<Value_array_time>(ctx,vLog__DocRefs__ah).arrayElem(ctx,llog__i.get()).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=38;
		if (g->Call(ctx,(PCV[]){nullptr,Ref(ctx,vLog__DocRefs__ah).cv(),llog__i.cv()},2,228)) goto _0;
		c.f.fLine=39;
		if (g->Call(ctx,(PCV[]){nullptr,Ref(ctx,vLog__Paths__at).cv(),llog__i.cv()},2,228)) goto _0;
		{
			Time t18;
			t18=lDocRef__h.get();
			c.f.fLine=41;
			if (g->Call(ctx,(PCV[]){nullptr,t18.cv()},1,267)) goto _0;
			g->Check(ctx);
		}
_5:
_4:
_3:
		goto _6;
_2:
		llog__i=1;
		{
			Long t19;
			t19=Var<Value_array_time>(ctx,vLog__DocRefs__ah).size();
			v0=t19.get();
		}
		goto _7;
_9:
		{
			Time t20;
			c.f.fLine=50;
			t20=Var<Value_array_time>(ctx,vLog__DocRefs__ah).arrayElem(ctx,llog__i.get()).get();
			if (g->Call(ctx,(PCV[]){nullptr,t20.cv()},1,267)) goto _0;
			g->Check(ctx);
		}
		if (ctx->doingAbort) goto _0;
_8:
		llog__i=llog__i.get()+1;
_7:
		if (llog__i.get()<=v0.get()) goto _9;
_10:
		c.f.fLine=53;
		if (g->Call(ctx,(PCV[]){nullptr,Ref(ctx,vLog__Paths__at).cv(),Long(0).cv()},2,222)) goto _0;
		c.f.fLine=54;
		if (g->Call(ctx,(PCV[]){nullptr,Ref(ctx,vLog__DocRefs__ah).cv(),Long(0).cv()},2,1223)) goto _0;
		g->Check(ctx);
_6:
_0:
_1:
;
	}

}
