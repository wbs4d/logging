extern Txt K;
extern Txt KAttributes_3A_20;
extern Txt KCourier;
extern Txt KCreated_20by;
extern Txt KDocumentation;
extern Txt KInvisible_2C_20;
extern Txt KMethods;
extern Txt KParameters_3A;
extern Txt KReturns_3A;
extern Txt KSQL_2C_20;
extern Txt KServer_2C_20;
extern Txt KShared_2C_20;
extern Txt KSoap_2C_20;
extern Txt KWSDL_2C_20;
extern Txt KWeb_2C_20;
extern Txt K_0D_0D;
extern Txt K_20;
extern Txt K_20_20_2F_2F_20;
extern Txt K_20_3A_20;
extern Txt K_23_23_20;
extern Txt K_24;
extern Txt K_2A_2AReturns_2A_2A;
extern Txt K_2D_2D_3E;
extern Txt K_2D_3E;
extern Txt K_2F;
extern Txt K_2F_2F;
extern Txt K_2F_2F_20;
extern Txt K_2F_2F_20Parameters_3A;
extern Txt K_2F_2F_20Returns_3A;
extern Txt K_2F_2F_20_20_20_20_24;
extern Txt K_2F_2F_20_20_20_24;
extern Txt K_2F_2F_20_20_24;
extern Txt K_2F_2F_20_24;
extern Txt K_3A;
extern Txt K_3C_21_2D_2D;
extern Txt K_40;
extern Txt K_40Created_20by_40;
extern Txt K_40Parameters_3A_40;
extern Txt K_40Returns_40;
extern Txt K_7C;
extern Txt Kcapable;
extern Txt Kincapable;
extern Txt Kindifferent;
extern Txt Kinvisible;
extern Txt Kpreemptive;
extern Txt KpublishedSoap;
extern Txt KpublishedSql;
extern Txt KpublishedWeb;
extern Txt KpublishedWsdl;
extern Txt Kshared;
extern Txt k$tdAxHfqfk0;
extern Txt k2ucr6KMltt4;
extern Txt kFztatg2AeEk;
extern Txt kHSK4e$VtNdY;
extern Txt kKqLvphAEyfI;
extern Txt kPk$4ELz88bk;
extern Txt kXztOPunYs9g;
extern Txt kasCU9KRLhsc;
extern Txt kbE413fCVMjA;
extern Txt kc$4IxpZB99g;
extern Txt kdAQ7fWUqce8;
extern Txt keYqLvjgs1KI;
extern Txt kemoKRlUtXYg;
extern Txt khj46qayLLyM;
extern Txt kq2BFNIvkFYU;
extern Txt kvUWSXMd0W2g;
extern Txt kx4DgsGRJUpg;
extern Txt kyUKfCm0Vmhg;
extern unsigned char D_proc_UTIL__WRITEMETHODCOMMENTS[];
void proc_UTIL__WRITEMETHODCOMMENTS( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_UTIL__WRITEMETHODCOMMENTS);
	if (!ctx->doingAbort) {
		Long v0;
		Long v1;
		Long lProcessID__i;
		Long v2;
		Long lNumberOfMethods__i;
		Txt lMethodName__t;
		Txt lMethodCode__t;
		Long lPosition__i;
		Value_array_text lMethodNames__at;
		Long v3;
		Long lline__i;
		Txt lSpace;
		Bool lexcludePrivate__b;
		Value_array_text lmethodLines__at;
		Long v4;
		Long llineEnd__i;
		Txt lcallSyntaxParameters__t;
		Value_array_text lMethodComments__at;
		Long v5;
		Txt llastChar__t;
		Long lparameterBlock__i;
		Txt lparameterBlock__t;
		Txt ldocumentationPath__t;
		Long lStackSize__i;
		Txt lCR;
		Value_array_text lMethodCode__at;
		Long lreturns__i;
		Txt lFirstChars__t;
		Long lCurrentMethod__i;
		Txt lprocessName__t;
		Long lnumberofLines__i;
		Bool lToolTip__b;
		Obj lAttributes__o;
		Long lnextLine__i;
		Txt lAttributes__t;
		Txt lparameterline__t;
		Txt lcallSyntax__t;
		Txt lnextline__t;
		if (!(Bool(0).get())) goto _2;
_2:
		{
			Ref t0;
			t0.setLocalRef(ctx,lMethodCode__at.cv());
			c.f.fLine=38;
			if (g->Call(ctx,(PCV[]){nullptr,t0.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ref t1;
			t1.setLocalRef(ctx,lmethodLines__at.cv());
			c.f.fLine=39;
			if (g->Call(ctx,(PCV[]){nullptr,t1.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ref t2;
			t2.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=40;
			if (g->Call(ctx,(PCV[]){nullptr,t2.cv(),Long(0).cv()},2,222)) goto _0;
		}
		lprocessName__t=kasCU9KRLhsc.get();
		lStackSize__i=0;
		{
			Txt t3;
			c.f.fLine=45;
			if (g->Call(ctx,(PCV[]){t3.cv()},0,1392)) goto _0;
			g->Check(ctx);
			Bool t4;
			t4=g->CompareString(ctx,t3.get(),lprocessName__t.get())==0;
			if (!(t4.get())) goto _3;
		}
		{
			Txt t5;
			c.f.fLine=47;
			if (g->Call(ctx,(PCV[]){t5.cv(),Long(13).cv()},1,90)) goto _0;
			lCR=t5.get();
		}
		lSpace=K_20.get();
		{
			Ref t6;
			t6.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=50;
			if (g->Call(ctx,(PCV[]){nullptr,Long(1).cv(),t6.cv()},2,1163)) goto _0;
			g->Check(ctx);
		}
		c.f.fLine=52;
		lMethodName__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=53;
		lToolTip__b=Parm<Bool>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=54;
		lexcludePrivate__b=Parm<Bool>(ctx,inParams,inNbParam,3).get();
		if (ctx->doingAbort) goto _0;
		{
			Long t7;
			t7=lMethodName__t.get().fLength;
			if (0>=t7.get()) goto _4;
		}
		{
			Ref t9;
			t9.setLocalRef(ctx,lMethodNames__at.cv());
			Long t10;
			c.f.fLine=58;
			if (g->Call(ctx,(PCV[]){t10.cv(),t9.cv(),lMethodName__t.cv()},2,907)) goto _0;
			g->Check(ctx);
			lNumberOfMethods__i=t10.get();
		}
		if (1!=lNumberOfMethods__i.get()) goto _5;
		{
			Ref t12;
			t12.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=61;
			if (g->Call(ctx,(PCV[]){nullptr,t12.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ref t13;
			t13.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=62;
			if (g->Call(ctx,(PCV[]){nullptr,t13.cv(),lMethodName__t.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		goto _6;
_5:
		{
			Ref t14;
			t14.setLocalRef(ctx,lMethodNames__at.cv());
			Long t15;
			c.f.fLine=65;
			if (g->Call(ctx,(PCV[]){t15.cv(),t14.cv()},1,274)) goto _0;
			lNumberOfMethods__i=t15.get();
		}
		lCurrentMethod__i=lNumberOfMethods__i.get();
		v0=1;
		goto _7;
_9:
		{
			Txt t16;
			g->AddString(lMethodName__t.get(),K_40.get(),t16.get());
			Txt t18;
			c.f.fLine=67;
			t18=lMethodNames__at.arrayElem(ctx,lCurrentMethod__i.get()).get();
			Bool t17;
			t17=g->CompareString(ctx,t18.get(),t16.get())==0;
			if (!(t17.get())) goto _11;
		}
		if (ctx->doingAbort) goto _0;
		goto _12;
_11:
		{
			Ref t19;
			t19.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=69;
			if (g->Call(ctx,(PCV[]){nullptr,t19.cv(),lCurrentMethod__i.cv()},2,228)) goto _0;
		}
_12:
_8:
		lCurrentMethod__i=lCurrentMethod__i.get()+-1;
_7:
		if (lCurrentMethod__i.get()>=v0.get()) goto _9;
_10:
_6:
_4:
		{
			Ref t22;
			t22.setLocalRef(ctx,lMethodNames__at.cv());
			Long t23;
			c.f.fLine=78;
			if (g->Call(ctx,(PCV[]){t23.cv(),t22.cv()},1,274)) goto _0;
			lNumberOfMethods__i=t23.get();
		}
		{
			Ref t24;
			t24.setLocalRef(ctx,lMethodCode__at.cv());
			Ref t25;
			t25.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=80;
			if (g->Call(ctx,(PCV[]){nullptr,t25.cv(),t24.cv()},2,1190)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t26;
			t26.setLocalRef(ctx,lMethodComments__at.cv());
			c.f.fLine=82;
			if (g->Call(ctx,(PCV[]){nullptr,t26.cv(),lNumberOfMethods__i.cv()},2,222)) goto _0;
		}
		lCurrentMethod__i=1;
		v2=lNumberOfMethods__i.get();
		goto _13;
_15:
		c.f.fLine=86;
		lMethodName__t=lMethodNames__at.arrayElem(ctx,lCurrentMethod__i.get()).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=88;
		lMethodCode__t=lMethodCode__at.arrayElem(ctx,lCurrentMethod__i.get()).get();
		if (ctx->doingAbort) goto _0;
		{
			Num t29;
			t29=32000;
			Ref t30;
			t30.setLocalRef(ctx,lmethodLines__at.cv());
			c.f.fLine=90;
			if (g->Call(ctx,(PCV[]){nullptr,lMethodCode__t.cv(),t30.cv(),t29.cv(),KCourier.cv(),Long(9).cv()},5,1149)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t31;
			t31.setLocalRef(ctx,lmethodLines__at.cv());
			c.f.fLine=93;
			if (g->Call(ctx,(PCV[]){nullptr,t31.cv(),Long(1).cv(),Long(1).cv()},3,228)) goto _0;
		}
		{
			Long t32;
			c.f.fLine=94;
			if (g->Call(ctx,(PCV[]){t32.cv(),kq2BFNIvkFYU.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lPosition__i=t32.get();
		}
		{
			Long t33;
			t33=kq2BFNIvkFYU.get().fLength;
			Long t34;
			t34=lPosition__i.get()+t33.get();
			Txt t35;
			c.f.fLine=95;
			if (g->Call(ctx,(PCV[]){t35.cv(),lMethodCode__t.cv(),t34.cv()},2,12)) goto _0;
			lMethodCode__t=t35.get();
		}
		{
			Long t36;
			c.f.fLine=98;
			if (g->Call(ctx,(PCV[]){t36.cv(),KCreated_20by.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lPosition__i=t36.get();
		}
		{
			Long t37;
			t37=lPosition__i.get()-3;
			Txt t38;
			c.f.fLine=99;
			if (g->Call(ctx,(PCV[]){t38.cv(),lMethodCode__t.cv(),Long(1).cv(),t37.cv()},3,12)) goto _0;
			lMethodCode__t=t38.get();
		}
		{
			Ref t39;
			t39.setLocalRef(ctx,lmethodLines__at.cv());
			Long t40;
			c.f.fLine=100;
			if (g->Call(ctx,(PCV[]){t40.cv(),t39.cv(),K_40Created_20by_40.cv()},2,230)) goto _0;
			lline__i=t40.get();
		}
		if (0>=lline__i.get()) goto _17;
		{
			Ref t42;
			t42.setLocalRef(ctx,lmethodLines__at.cv());
			c.f.fLine=102;
			if (g->Call(ctx,(PCV[]){nullptr,t42.cv(),lline__i.cv(),Long(32000).cv()},3,228)) goto _0;
		}
_17:
		{
			Txt t43;
			c.f.fLine=106;
			if (g->Call(ctx,(PCV[]){t43.cv(),lMethodCode__t.cv(),kPk$4ELz88bk.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t43.get();
		}
		{
			Txt t44;
			c.f.fLine=107;
			if (g->Call(ctx,(PCV[]){t44.cv(),lMethodCode__t.cv(),kHSK4e$VtNdY.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t44.get();
		}
		{
			Long t45;
			c.f.fLine=110;
			if (g->Call(ctx,(PCV[]){t45.cv(),KParameters_3A.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lparameterBlock__i=t45.get();
		}
		if (0>=lparameterBlock__i.get()) goto _18;
		{
			Long t47;
			c.f.fLine=112;
			if (g->Call(ctx,(PCV[]){t47.cv(),kbE413fCVMjA.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			if (0>=t47.get()) goto _19;
		}
		lparameterBlock__i=0;
_19:
_18:
		{
			Long t49;
			c.f.fLine=118;
			if (g->Call(ctx,(PCV[]){t49.cv(),KReturns_3A.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lreturns__i=t49.get();
		}
		if (0>=lreturns__i.get()) goto _20;
		{
			Long t51;
			c.f.fLine=120;
			if (g->Call(ctx,(PCV[]){t51.cv(),kc$4IxpZB99g.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			if (0>=t51.get()) goto _21;
		}
		lreturns__i=0;
_21:
_20:
		{
			Txt t53;
			g->AddString(kemoKRlUtXYg.get(),lCR.get(),t53.get());
			Txt t54;
			g->AddString(t53.get(),kXztOPunYs9g.get(),t54.get());
			g->AddString(t54.get(),lCR.get(),lparameterBlock__t.get());
		}
		{
			Ref t56;
			t56.setLocalRef(ctx,lmethodLines__at.cv());
			Long t57;
			c.f.fLine=127;
			if (g->Call(ctx,(PCV[]){t57.cv(),t56.cv()},1,274)) goto _0;
			lnumberofLines__i=t57.get();
		}
		{
			Bool t58;
			t58=0<lparameterBlock__i.get();
			Bool t59;
			t59=0<lreturns__i.get();
			if (!( t58.get()&&t59.get())) goto _23;
		}
		{
			Ref t61;
			t61.setLocalRef(ctx,lmethodLines__at.cv());
			Long t62;
			c.f.fLine=130;
			if (g->Call(ctx,(PCV[]){t62.cv(),t61.cv(),K_40Parameters_3A_40.cv()},2,230)) goto _0;
			lparameterBlock__i=t62.get()+1;
		}
		c.f.fLine=131;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
_25:
		{
			Txt t65;
			c.f.fLine=134;
			if (g->Call(ctx,(PCV[]){t65.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t65.get();
		}
		{
			Txt t66;
			c.f.fLine=135;
			if (g->Call(ctx,(PCV[]){t66.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t66.get();
		}
		{
			Txt t67;
			c.f.fLine=136;
			if (g->Call(ctx,(PCV[]){t67.cv(),lparameterline__t.cv(),K_2F_2F_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t67.get();
		}
		{
			Txt t68;
			c.f.fLine=137;
			if (g->Call(ctx,(PCV[]){t68.cv(),lparameterline__t.cv(),K_2F_2F_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t68.get();
		}
		{
			Txt t69;
			c.f.fLine=138;
			if (g->Call(ctx,(PCV[]){t69.cv(),lparameterline__t.cv(),K_20_3A_20.cv(),K_7C.cv()},3,233)) goto _0;
			lparameterline__t=t69.get();
		}
		{
			Txt t70;
			g->AddString(lparameterBlock__t.get(),lparameterline__t.get(),t70.get());
			g->AddString(t70.get(),lCR.get(),lparameterBlock__t.get());
		}
		lparameterBlock__i=lparameterBlock__i.get()+1;
		c.f.fLine=143;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
_24:
		{
			Bool t74;
			t74=g->CompareString(ctx,lparameterline__t.get(),K_40Returns_40.get())==0;
			if (!(t74.get())) goto _25;
		}
_26:
		goto _22;
_23:
		if (0>=lparameterBlock__i.get()) goto _27;
		{
			Ref t76;
			t76.setLocalRef(ctx,lmethodLines__at.cv());
			Long t77;
			c.f.fLine=147;
			if (g->Call(ctx,(PCV[]){t77.cv(),t76.cv(),K_40Parameters_3A_40.cv()},2,230)) goto _0;
			lparameterBlock__i=t77.get()+1;
		}
		c.f.fLine=148;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
_29:
		{
			Txt t80;
			c.f.fLine=151;
			if (g->Call(ctx,(PCV[]){t80.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t80.get();
		}
		{
			Txt t81;
			c.f.fLine=152;
			if (g->Call(ctx,(PCV[]){t81.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t81.get();
		}
		{
			Txt t82;
			c.f.fLine=153;
			if (g->Call(ctx,(PCV[]){t82.cv(),lparameterline__t.cv(),K_2F_2F_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t82.get();
		}
		{
			Txt t83;
			c.f.fLine=154;
			if (g->Call(ctx,(PCV[]){t83.cv(),lparameterline__t.cv(),K_2F_2F_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t83.get();
		}
		{
			Txt t84;
			c.f.fLine=155;
			if (g->Call(ctx,(PCV[]){t84.cv(),lparameterline__t.cv(),K_20_3A_20.cv(),K_7C.cv()},3,233)) goto _0;
			lparameterline__t=t84.get();
		}
		{
			Txt t85;
			g->AddString(lparameterBlock__t.get(),lparameterline__t.get(),t85.get());
			g->AddString(t85.get(),lCR.get(),lparameterBlock__t.get());
		}
		lparameterBlock__i=lparameterBlock__i.get()+1;
		if (lparameterBlock__i.get()>lnumberofLines__i.get()) goto _31;
		c.f.fLine=162;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
		goto _32;
_31:
		lparameterline__t=K.get();
_32:
		lnextLine__i=lparameterBlock__i.get()+1;
		if (lnextLine__i.get()>lnumberofLines__i.get()) goto _33;
		c.f.fLine=169;
		lnextline__t=lmethodLines__at.arrayElem(ctx,lnextLine__i.get()).get();
		if (ctx->doingAbort) goto _0;
		goto _34;
_33:
		lnextline__t=K.get();
_34:
_28:
		{
			Bool t93;
			t93=lparameterBlock__i.get()>lnumberofLines__i.get();
			Bool t94;
			t94=g->CompareString(ctx,lparameterline__t.get(),K.get())==0;
			Bool t95;
			t95=g->CompareString(ctx,lnextline__t.get(),K.get())==0;
			Bool t96;
			t96=t94.get()&&t95.get();
			Bool t97;
			t97=t93.get()||t96.get();
			Bool t98;
			t98=g->CompareString(ctx,lparameterline__t.get(),K_40Returns_40.get())==0;
			if (!( t97.get()||t98.get())) goto _29;
		}
_30:
		goto _22;
_27:
		if (0>=lreturns__i.get()) goto _35;
		goto _22;
_35:
		lparameterBlock__t=K.get();
_22:
		{
			Txt t101;
			g->AddString(lCR.get(),lCR.get(),t101.get());
			Txt t102;
			c.f.fLine=189;
			if (g->Call(ctx,(PCV[]){t102.cv(),lparameterBlock__t.cv(),t101.cv(),lCR.cv()},3,233)) goto _0;
			lparameterBlock__t=t102.get();
		}
		if (0>=lreturns__i.get()) goto _36;
		c.f.fLine=192;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
_38:
		{
			Txt t105;
			c.f.fLine=195;
			if (g->Call(ctx,(PCV[]){t105.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t105.get();
		}
		{
			Txt t106;
			c.f.fLine=196;
			if (g->Call(ctx,(PCV[]){t106.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t106.get();
		}
		{
			Txt t107;
			c.f.fLine=197;
			if (g->Call(ctx,(PCV[]){t107.cv(),lparameterline__t.cv(),K_2F_2F_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t107.get();
		}
		{
			Txt t108;
			c.f.fLine=198;
			if (g->Call(ctx,(PCV[]){t108.cv(),lparameterline__t.cv(),K_2F_2F_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t108.get();
		}
		{
			Txt t109;
			c.f.fLine=199;
			if (g->Call(ctx,(PCV[]){t109.cv(),lparameterline__t.cv(),K_20_3A_20.cv(),K_7C.cv()},3,233)) goto _0;
			lparameterline__t=t109.get();
		}
		{
			Txt t110;
			g->AddString(lparameterBlock__t.get(),lparameterline__t.get(),t110.get());
			g->AddString(t110.get(),lCR.get(),lparameterBlock__t.get());
		}
		lparameterBlock__i=lparameterBlock__i.get()+1;
		if (lparameterBlock__i.get()>lnumberofLines__i.get()) goto _40;
		c.f.fLine=206;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
		goto _41;
_40:
		lparameterline__t=K.get();
_41:
		lnextLine__i=lparameterBlock__i.get()+1;
		if (lnextLine__i.get()>lnumberofLines__i.get()) goto _42;
		c.f.fLine=213;
		lnextline__t=lmethodLines__at.arrayElem(ctx,lnextLine__i.get()).get();
		if (ctx->doingAbort) goto _0;
		goto _43;
_42:
		lnextline__t=K.get();
_43:
_37:
		{
			Bool t118;
			t118=lparameterBlock__i.get()>lnumberofLines__i.get();
			Bool t119;
			t119=g->CompareString(ctx,lparameterline__t.get(),K.get())==0;
			Bool t120;
			t120=g->CompareString(ctx,lnextline__t.get(),K.get())==0;
			Bool t121;
			t121=t119.get()&&t120.get();
			if (!( t118.get()||t121.get())) goto _38;
		}
_39:
_36:
		{
			Txt t123;
			g->AddString(lCR.get(),lCR.get(),t123.get());
			Txt t124;
			c.f.fLine=228;
			if (g->Call(ctx,(PCV[]){t124.cv(),lparameterBlock__t.cv(),t123.cv(),lCR.cv()},3,233)) goto _0;
			lparameterBlock__t=t124.get();
		}
		{
			Long t125;
			c.f.fLine=231;
			if (g->Call(ctx,(PCV[]){t125.cv(),K_2F_2F_20Parameters_3A.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lparameterBlock__i=t125.get();
		}
		{
			Long t126;
			c.f.fLine=232;
			if (g->Call(ctx,(PCV[]){t126.cv(),K_2F_2F_20Returns_3A.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lreturns__i=t126.get();
		}
		if (0>=lparameterBlock__i.get()) goto _44;
		{
			Txt t128;
			c.f.fLine=235;
			if (g->Call(ctx,(PCV[]){t128.cv(),lMethodCode__t.cv(),Long(1).cv(),lparameterBlock__i.cv()},3,12)) goto _0;
			lMethodCode__t=t128.get();
		}
		goto _45;
_44:
		if (0>=lreturns__i.get()) goto _46;
		{
			Txt t130;
			c.f.fLine=238;
			if (g->Call(ctx,(PCV[]){t130.cv(),lMethodCode__t.cv(),Long(1).cv(),lreturns__i.cv()},3,12)) goto _0;
			lMethodCode__t=t130.get();
		}
_46:
_45:
		{
			Ref t131;
			t131.setLocalRef(ctx,lAttributes__o.cv());
			Ref t132;
			t132.setLocalRef(ctx,lMethodName__t.cv());
			c.f.fLine=243;
			if (g->Call(ctx,(PCV[]){nullptr,t132.cv(),t131.cv()},2,1334)) goto _0;
			g->Check(ctx);
		}
		lAttributes__t=KAttributes_3A_20.get();
		{
			Variant t133;
			c.f.fLine=247;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kshared.cv(),t133.cv())) goto _0;
			Bool t134;
			if (!g->GetValue(ctx,(PCV[]){t134.cv(),t133.cv(),nullptr})) goto _0;
			if (!(t134.get())) goto _47;
		}
		g->AddString(lAttributes__t.get(),KShared_2C_20.get(),lAttributes__t.get());
_47:
		{
			Variant t136;
			c.f.fLine=251;
			if (g->GetMember(ctx,lAttributes__o.cv(),kx4DgsGRJUpg.cv(),t136.cv())) goto _0;
			Bool t137;
			if (!g->GetValue(ctx,(PCV[]){t137.cv(),t136.cv(),nullptr})) goto _0;
			if (!(t137.get())) goto _48;
		}
		g->AddString(lAttributes__t.get(),KServer_2C_20.get(),lAttributes__t.get());
_48:
		{
			Variant t139;
			c.f.fLine=255;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kinvisible.cv(),t139.cv())) goto _0;
			Bool t140;
			if (!g->GetValue(ctx,(PCV[]){t140.cv(),t139.cv(),nullptr})) goto _0;
			if (!(t140.get())) goto _49;
		}
		g->AddString(lAttributes__t.get(),KInvisible_2C_20.get(),lAttributes__t.get());
_49:
		{
			Variant t142;
			c.f.fLine=266;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kpreemptive.cv(),t142.cv())) goto _0;
			Bool t143;
			if (g->OperationOnAny(ctx,6,t142.cv(),Kcapable.cv(),t143.cv())) goto _0;
			if (!(t143.get())) goto _51;
		}
		g->AddString(lAttributes__t.get(),khj46qayLLyM.get(),lAttributes__t.get());
		goto _50;
_51:
		{
			Variant t145;
			c.f.fLine=269;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kpreemptive.cv(),t145.cv())) goto _0;
			Bool t146;
			if (g->OperationOnAny(ctx,6,t145.cv(),Kincapable.cv(),t146.cv())) goto _0;
			if (!(t146.get())) goto _52;
		}
		g->AddString(lAttributes__t.get(),keYqLvjgs1KI.get(),lAttributes__t.get());
		goto _50;
_52:
		{
			Variant t148;
			c.f.fLine=272;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kpreemptive.cv(),t148.cv())) goto _0;
			Bool t149;
			if (g->OperationOnAny(ctx,6,t148.cv(),Kindifferent.cv(),t149.cv())) goto _0;
			if (!(t149.get())) goto _53;
		}
		g->AddString(lAttributes__t.get(),k$tdAxHfqfk0.get(),lAttributes__t.get());
		goto _50;
_53:
_50:
		{
			Variant t151;
			c.f.fLine=277;
			if (g->GetMember(ctx,lAttributes__o.cv(),KpublishedSoap.cv(),t151.cv())) goto _0;
			Bool t152;
			if (!g->GetValue(ctx,(PCV[]){t152.cv(),t151.cv(),nullptr})) goto _0;
			if (!(t152.get())) goto _54;
		}
		g->AddString(lAttributes__t.get(),KSoap_2C_20.get(),lAttributes__t.get());
_54:
		{
			Variant t154;
			c.f.fLine=281;
			if (g->GetMember(ctx,lAttributes__o.cv(),KpublishedSql.cv(),t154.cv())) goto _0;
			Bool t155;
			if (!g->GetValue(ctx,(PCV[]){t155.cv(),t154.cv(),nullptr})) goto _0;
			if (!(t155.get())) goto _55;
		}
		g->AddString(lAttributes__t.get(),KSQL_2C_20.get(),lAttributes__t.get());
_55:
		{
			Variant t157;
			c.f.fLine=285;
			if (g->GetMember(ctx,lAttributes__o.cv(),KpublishedWeb.cv(),t157.cv())) goto _0;
			Bool t158;
			if (!g->GetValue(ctx,(PCV[]){t158.cv(),t157.cv(),nullptr})) goto _0;
			if (!(t158.get())) goto _56;
		}
		g->AddString(lAttributes__t.get(),KWeb_2C_20.get(),lAttributes__t.get());
_56:
		{
			Variant t160;
			c.f.fLine=289;
			if (g->GetMember(ctx,lAttributes__o.cv(),KpublishedWsdl.cv(),t160.cv())) goto _0;
			Bool t161;
			if (!g->GetValue(ctx,(PCV[]){t161.cv(),t160.cv(),nullptr})) goto _0;
			if (!(t161.get())) goto _57;
		}
		g->AddString(lAttributes__t.get(),KWSDL_2C_20.get(),lAttributes__t.get());
_57:
		{
			Long t163;
			t163=lAttributes__t.get().fLength;
			if (1>=t163.get()) goto _58;
		}
		{
			Long t165;
			t165=lAttributes__t.get().fLength;
			Long t166;
			t166=t165.get()-2;
			Txt t167;
			c.f.fLine=294;
			if (g->Call(ctx,(PCV[]){t167.cv(),lAttributes__t.cv(),Long(1).cv(),t166.cv()},3,12)) goto _0;
			Txt t168;
			g->AddString(t167.get(),lCR.get(),t168.get());
			g->AddString(t168.get(),lCR.get(),lAttributes__t.get());
		}
_58:
		{
			Long t170;
			c.f.fLine=297;
			if (g->Call(ctx,(PCV[]){t170.cv(),kvUWSXMd0W2g.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lPosition__i=t170.get();
		}
		lPosition__i=lPosition__i.get()+15;
		{
			Long t172;
			c.f.fLine=299;
			if (g->Call(ctx,(PCV[]){t172.cv(),lCR.cv(),lMethodCode__t.cv(),lPosition__i.cv()},3,15)) goto _0;
			llineEnd__i=t172.get();
		}
		{
			Long t173;
			t173=llineEnd__i.get()-lPosition__i.get();
			Txt t174;
			c.f.fLine=301;
			if (g->Call(ctx,(PCV[]){t174.cv(),lMethodCode__t.cv(),lPosition__i.cv(),t173.cv()},3,12)) goto _0;
			lcallSyntax__t=t174.get();
		}
_59:
		{
			Txt t175;
			c.f.fLine=303;
			if (g->Call(ctx,(PCV[]){t175.cv(),lcallSyntax__t.cv(),Long(1).cv(),Long(1).cv()},3,12)) goto _0;
			Bool t176;
			t176=g->CompareString(ctx,t175.get(),lSpace.get())==0;
			if (!(t176.get())) goto _60;
		}
		{
			Txt t177;
			c.f.fLine=304;
			if (g->Call(ctx,(PCV[]){t177.cv(),lcallSyntax__t.cv(),Long(2).cv()},2,12)) goto _0;
			lcallSyntax__t=t177.get();
		}
		goto _59;
_60:
		{
			Txt t178;
			c.f.fLine=307;
			if (g->Call(ctx,(PCV[]){t178.cv(),lcallSyntax__t.cv(),K_2D_2D_3E.cv(),K_2D_3E.cv()},3,233)) goto _0;
			lcallSyntax__t=t178.get();
		}
		lcallSyntaxParameters__t=lparameterBlock__t.get();
		{
			Txt t179;
			c.f.fLine=311;
			if (g->Call(ctx,(PCV[]){t179.cv(),lcallSyntaxParameters__t.cv(),kXztOPunYs9g.cv(),K.cv()},3,233)) goto _0;
			lcallSyntaxParameters__t=t179.get();
		}
		{
			Txt t180;
			c.f.fLine=312;
			if (g->Call(ctx,(PCV[]){t180.cv(),lcallSyntaxParameters__t.cv(),K_7C.cv(),K_20_3A_20.cv()},3,233)) goto _0;
			lcallSyntaxParameters__t=t180.get();
		}
		{
			Txt t181;
			g->AddString(lcallSyntax__t.get(),lCR.get(),t181.get());
			g->AddString(t181.get(),lcallSyntaxParameters__t.get(),lcallSyntax__t.get());
		}
		{
			Txt t183;
			c.f.fLine=317;
			if (g->Call(ctx,(PCV[]){t183.cv(),lcallSyntax__t.cv(),k2ucr6KMltt4.cv(),K.cv()},3,233)) goto _0;
			lcallSyntax__t=t183.get();
		}
		{
			Txt t184;
			c.f.fLine=318;
			if (g->Call(ctx,(PCV[]){t184.cv(),lcallSyntax__t.cv(),K_2F_2F_20Returns_3A.cv(),K.cv()},3,233)) goto _0;
			lcallSyntax__t=t184.get();
		}
		{
			Txt t185;
			g->AddString(lCR.get(),lCR.get(),t185.get());
			Txt t186;
			c.f.fLine=319;
			if (g->Call(ctx,(PCV[]){t186.cv(),lcallSyntax__t.cv(),t185.cv(),lCR.cv()},3,233)) goto _0;
			lcallSyntax__t=t186.get();
		}
		{
			Long t187;
			t187=lcallSyntax__t.get().fLength;
			Long t188;
			t188=t187.get()-1;
			Txt t189;
			c.f.fLine=320;
			if (g->Call(ctx,(PCV[]){t189.cv(),lcallSyntax__t.cv(),Long(1).cv(),t188.cv()},3,12)) goto _0;
			lcallSyntax__t=t189.get();
		}
		{
			Txt t190;
			c.f.fLine=322;
			if (g->Call(ctx,(PCV[]){t190.cv(),lcallSyntax__t.cv(),Long(1).cv(),Long(1).cv()},3,12)) goto _0;
			lFirstChars__t=t190.get();
		}
_61:
		{
			Bool t191;
			t191=g->CompareString(ctx,lFirstChars__t.get(),K_20.get())==0;
			if (!(t191.get())) goto _62;
		}
		{
			Txt t192;
			c.f.fLine=324;
			if (g->Call(ctx,(PCV[]){t192.cv(),lcallSyntax__t.cv(),Long(2).cv()},2,12)) goto _0;
			lcallSyntax__t=t192.get();
		}
		{
			Txt t193;
			c.f.fLine=325;
			if (g->Call(ctx,(PCV[]){t193.cv(),lcallSyntax__t.cv(),Long(1).cv(),Long(1).cv()},3,12)) goto _0;
			lFirstChars__t=t193.get();
		}
		goto _61;
_62:
		{
			Txt t194;
			g->AddString(lCR.get(),lAttributes__t.get(),t194.get());
			Txt t195;
			c.f.fLine=328;
			if (g->Call(ctx,(PCV[]){t195.cv(),lMethodCode__t.cv(),kdAQ7fWUqce8.cv(),t194.cv()},3,233)) goto _0;
			lMethodCode__t=t195.get();
		}
		{
			Txt t196;
			g->AddString(lCR.get(),lAttributes__t.get(),t196.get());
			Txt t197;
			c.f.fLine=329;
			if (g->Call(ctx,(PCV[]){t197.cv(),lMethodCode__t.cv(),kKqLvphAEyfI.cv(),t196.cv()},3,233)) goto _0;
			lMethodCode__t=t197.get();
		}
		{
			Txt t198;
			c.f.fLine=333;
			if (g->Call(ctx,(PCV[]){t198.cv(),lMethodCode__t.cv(),kFztatg2AeEk.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t198.get();
		}
		{
			Txt t199;
			c.f.fLine=334;
			if (g->Call(ctx,(PCV[]){t199.cv(),lMethodCode__t.cv(),kyUKfCm0Vmhg.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t199.get();
		}
		{
			Txt t200;
			c.f.fLine=336;
			if (g->Call(ctx,(PCV[]){t200.cv(),lMethodCode__t.cv(),K_20_20_2F_2F_20.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t200.get();
		}
		{
			Txt t201;
			c.f.fLine=337;
			if (g->Call(ctx,(PCV[]){t201.cv(),lMethodCode__t.cv(),K_2F_2F_20.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t201.get();
		}
		{
			Txt t202;
			g->AddString(lMethodCode__t.get(),lCR.get(),t202.get());
			g->AddString(t202.get(),lparameterBlock__t.get(),lMethodCode__t.get());
		}
		{
			Txt t204;
			c.f.fLine=341;
			if (g->Call(ctx,(PCV[]){t204.cv(),lMethodCode__t.cv(),Long(1).cv(),Long(2).cv()},3,12)) goto _0;
			lFirstChars__t=t204.get();
		}
_63:
		{
			Bool t205;
			t205=g->CompareString(ctx,lFirstChars__t.get(),K_0D_0D.get())==0;
			if (!(t205.get())) goto _64;
		}
		{
			Txt t206;
			c.f.fLine=343;
			if (g->Call(ctx,(PCV[]){t206.cv(),lMethodCode__t.cv(),Long(2).cv()},2,12)) goto _0;
			lMethodCode__t=t206.get();
		}
		{
			Txt t207;
			c.f.fLine=344;
			if (g->Call(ctx,(PCV[]){t207.cv(),lMethodCode__t.cv(),Long(1).cv(),Long(2).cv()},3,12)) goto _0;
			lFirstChars__t=t207.get();
		}
		goto _63;
_64:
		{
			Long t208;
			t208=lMethodCode__t.get().fLength;
			Txt t209;
			c.f.fLine=347;
			if (g->Call(ctx,(PCV[]){t209.cv(),lMethodCode__t.cv(),t208.cv(),Long(1).cv()},3,12)) goto _0;
			llastChar__t=t209.get();
		}
_65:
		{
			Bool t210;
			t210=g->CompareString(ctx,llastChar__t.get(),lCR.get())==0;
			Bool t211;
			t211=g->CompareString(ctx,llastChar__t.get(),lSpace.get())==0;
			if (!( t210.get()||t211.get())) goto _66;
		}
		{
			Long t213;
			t213=lMethodCode__t.get().fLength;
			Long t214;
			t214=t213.get()-1;
			Txt t215;
			c.f.fLine=349;
			if (g->Call(ctx,(PCV[]){t215.cv(),lMethodCode__t.cv(),Long(1).cv(),t214.cv()},3,12)) goto _0;
			lMethodCode__t=t215.get();
		}
		{
			Long t216;
			t216=lMethodCode__t.get().fLength;
			Txt t217;
			c.f.fLine=350;
			if (g->Call(ctx,(PCV[]){t217.cv(),lMethodCode__t.cv(),t216.cv(),Long(1).cv()},3,12)) goto _0;
			llastChar__t=t217.get();
		}
		goto _65;
_66:
		{
			Txt t218;
			c.f.fLine=353;
			if (g->Call(ctx,(PCV[]){t218.cv(),lMethodCode__t.cv(),KReturns_3A.cv(),K_2A_2AReturns_2A_2A.cv()},3,233)) goto _0;
			lMethodCode__t=t218.get();
		}
		{
			Txt t219;
			g->AddString(K_3C_21_2D_2D.get(),lcallSyntax__t.get(),t219.get());
			Txt t220;
			g->AddString(t219.get(),K_2D_2D_3E.get(),t220.get());
			Txt t221;
			g->AddString(t220.get(),lCR.get(),t221.get());
			Txt t222;
			g->AddString(t221.get(),K_23_23_20.get(),t222.get());
			Txt t223;
			g->AddString(t222.get(),lMethodName__t.get(),t223.get());
			Txt t224;
			g->AddString(t223.get(),lCR.get(),t224.get());
			g->AddString(t224.get(),lMethodCode__t.get(),lMethodCode__t.get());
		}
		{
			Txt t226;
			g->AddString(lCR.get(),K_2F_2F.get(),t226.get());
			Txt t227;
			c.f.fLine=357;
			if (g->Call(ctx,(PCV[]){t227.cv(),lMethodCode__t.cv(),t226.cv(),lCR.cv()},3,233)) goto _0;
			lMethodCode__t=t227.get();
		}
		{
			Txt t228;
			g->AddString(lCR.get(),K_2F.get(),t228.get());
			Txt t229;
			c.f.fLine=358;
			if (g->Call(ctx,(PCV[]){t229.cv(),lMethodCode__t.cv(),t228.cv(),lCR.cv()},3,233)) goto _0;
			lMethodCode__t=t229.get();
		}
		{
			Variant t230;
			c.f.fLine=360;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kshared.cv(),t230.cv())) goto _0;
			Bool t231;
			if (!g->GetValue(ctx,(PCV[]){t231.cv(),t230.cv(),nullptr})) goto _0;
			Bool t232;
			t232=!(t231.get());
			Bool t233;
			t233=lexcludePrivate__b.get()&&t232.get();
			if (!(t233.get())) goto _67;
		}
		lMethodCode__t=K.get();
_67:
		c.f.fLine=365;
		lMethodComments__at.arrayElem(ctx,lCurrentMethod__i.get())=lMethodCode__t.get();
		if (ctx->doingAbort) goto _0;
_14:
		lCurrentMethod__i=lCurrentMethod__i.get()+1;
_13:
		if (lCurrentMethod__i.get()<=v2.get()) goto _15;
_16:
		{
			Ref t237;
			t237.setLocalRef(ctx,lMethodComments__at.cv());
			Long t238;
			c.f.fLine=369;
			if (g->Call(ctx,(PCV[]){t238.cv(),t237.cv()},1,274)) goto _0;
			lNumberOfMethods__i=t238.get();
		}
		lCurrentMethod__i=lNumberOfMethods__i.get();
		v4=1;
		goto _68;
_70:
		{
			Txt t239;
			c.f.fLine=371;
			t239=lMethodComments__at.arrayElem(ctx,lCurrentMethod__i.get()).get();
			Txt t240;
			t240=t239.get();
			Long t241;
			t241=t240.get().fLength;
			Bool t242;
			t242=0<t241.get();
			if (!(t242.get())) goto _72;
		}
		if (ctx->doingAbort) goto _0;
		goto _73;
_72:
		{
			Ref t243;
			t243.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=373;
			if (g->Call(ctx,(PCV[]){nullptr,t243.cv(),lCurrentMethod__i.cv()},2,228)) goto _0;
		}
		{
			Ref t244;
			t244.setLocalRef(ctx,lMethodComments__at.cv());
			c.f.fLine=374;
			if (g->Call(ctx,(PCV[]){nullptr,t244.cv(),lCurrentMethod__i.cv()},2,228)) goto _0;
		}
_73:
_69:
		lCurrentMethod__i=lCurrentMethod__i.get()+-1;
_68:
		if (lCurrentMethod__i.get()>=v4.get()) goto _70;
_71:
		{
			Ref t247;
			t247.setLocalRef(ctx,lMethodComments__at.cv());
			Ref t248;
			t248.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=379;
			if (g->Call(ctx,(PCV[]){nullptr,t248.cv(),t247.cv()},2,1193)) goto _0;
			g->Check(ctx);
		}
		goto _74;
_3:
		lMethodName__t=K.get();
		lToolTip__b=Bool(1).get();
		lexcludePrivate__b=Bool(0).get();
		{
			Long t249;
			t249=inNbExplicitParam;
			if (1!=t249.get()) goto _76;
		}
		c.f.fLine=391;
		lMethodName__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		goto _75;
_76:
		{
			Long t251;
			t251=inNbExplicitParam;
			if (2!=t251.get()) goto _77;
		}
		c.f.fLine=394;
		lMethodName__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=395;
		lToolTip__b=Parm<Bool>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		goto _75;
_77:
		{
			Long t253;
			t253=inNbExplicitParam;
			if (3!=t253.get()) goto _78;
		}
		c.f.fLine=398;
		lMethodName__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=399;
		lToolTip__b=Parm<Bool>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=400;
		lexcludePrivate__b=Parm<Bool>(ctx,inParams,inNbParam,3).get();
		if (ctx->doingAbort) goto _0;
		goto _75;
_78:
_75:
		if (!(lexcludePrivate__b.get())) goto _79;
		{
			Txt t255;
			c.f.fLine=405;
			if (g->Call(ctx,(PCV[]){t255.cv(),Long(4).cv()},1,485)) goto _0;
			g->Check(ctx);
			Txt t256;
			g->AddString(t255.get(),KDocumentation.get(),t256.get());
			Txt t257;
			g->AddString(t256.get(),K_3A.get(),t257.get());
			Txt t258;
			g->AddString(t257.get(),KMethods.get(),t258.get());
			g->AddString(t258.get(),K_3A.get(),ldocumentationPath__t.get());
		}
		{
			Long t260;
			c.f.fLine=406;
			if (g->Call(ctx,(PCV[]){t260.cv(),ldocumentationPath__t.cv()},1,476)) goto _0;
			g->Check(ctx);
			if (0!=t260.get()) goto _80;
		}
		c.f.fLine=407;
		if (g->Call(ctx,(PCV[]){nullptr,ldocumentationPath__t.cv(),Long(1).cv()},2,693)) goto _0;
		g->Check(ctx);
_80:
		c.f.fLine=409;
		if (g->Call(ctx,(PCV[]){nullptr,ldocumentationPath__t.cv()},1,475)) goto _0;
		g->Check(ctx);
_79:
		{
			Txt t262;
			c.f.fLine=417;
			if (g->Call(ctx,(PCV[]){t262.cv()},0,684)) goto _0;
			g->Check(ctx);
			Bool t263;
			t263=lexcludePrivate__b.get();
			Bool t264;
			t264=lToolTip__b.get();
			Long t265;
			if (g->Call(ctx,(PCV[]){t265.cv(),t262.cv(),lStackSize__i.cv(),lprocessName__t.cv(),lMethodName__t.cv(),t264.cv(),t263.cv(),Ref((optyp)3).cv()},7,317)) goto _0;
			lProcessID__i=t265.get();
		}
		c.f.fLine=419;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,320)) goto _0;
		g->Check(ctx);
		c.f.fLine=420;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,325)) goto _0;
		g->Check(ctx);
		c.f.fLine=421;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,326)) goto _0;
		g->Check(ctx);
_74:
_0:
_1:
;
	}

}
