extern unsigned char D_proc_LOGUTIL__POSITIONR[];
void proc_LOGUTIL__POSITIONR( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOGUTIL__POSITIONR);
	if (!ctx->doingAbort) {
		Txt lmarker__t;
		Long lposNext__i;
		Long lposLast__i;
		new ( outResult) Long();
		{
			Txt t0;
			c.f.fLine=26;
			if (g->Call(ctx,(PCV[]){t0.cv(),Long(1).cv()},1,90)) goto _0;
			Long t1;
			t1=Parm<Txt>(ctx,inParams,inNbParam,1).get().fLength;
			Txt t2;
			g->MultiplyString(t0.get(),(sLONG)t1.get(),t2.get());
			lmarker__t=t2.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Bool t3;
			c.f.fLine=27;
			t3=g->CompareString(ctx,Parm<Txt>(ctx,inParams,inNbParam,1).get(),lmarker__t.get())==0;
			if (!(t3.get())) goto _2;
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t4;
			c.f.fLine=28;
			if (g->Call(ctx,(PCV[]){t4.cv(),Long(2).cv()},1,90)) goto _0;
			Long t5;
			t5=Parm<Txt>(ctx,inParams,inNbParam,1).get().fLength;
			Txt t6;
			g->MultiplyString(t4.get(),(sLONG)t5.get(),t6.get());
			lmarker__t=t6.get();
		}
		if (ctx->doingAbort) goto _0;
_2:
		lposLast__i=0;
_4:
		c.f.fLine=33;
		{
			Long t7;
			if (g->Call(ctx,(PCV[]){t7.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),Parm<Txt>(ctx,inParams,inNbParam,2).cv()},2,15)) goto _0;
			lposNext__i=t7.get();
		}
		if (ctx->doingAbort) goto _0;
		if (0>=lposNext__i.get()) goto _6;
		lposLast__i=lposNext__i.get();
		c.f.fLine=36;
		{
			Txt t9;
			if (g->Call(ctx,(PCV[]){t9.cv(),Parm<Txt>(ctx,inParams,inNbParam,2).cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),lmarker__t.cv(),Long(1).cv()},4,233)) goto _0;
			Parm<Txt>(ctx,inParams,inNbParam,2)=t9.get();
		}
		if (ctx->doingAbort) goto _0;
_6:
_3:
		if (0!=lposNext__i.get()) goto _4;
_5:
		c.f.fLine=40;
		Res<Long>(outResult)=lposLast__i.get();
		goto _0;
_0:
_1:
;
	}

}
