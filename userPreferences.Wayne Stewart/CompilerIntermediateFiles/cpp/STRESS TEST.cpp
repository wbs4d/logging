extern Txt K000;
extern Txt KFinished;
extern Txt KFri;
extern Txt KMon;
extern Txt KSat;
extern Txt KSun;
extern Txt KThu;
extern Txt KTue;
extern Txt KWed;
extern Txt K_20;
Asm4d_Proc proc_LOG_20ADD_20ENTRY;
Asm4d_Proc proc_LOG_20BUG_20ALERT;
Asm4d_Proc proc_LOG_20CLOSE_20LOG;
Asm4d_Proc proc_LOG_20ENABLE;
Asm4d_Proc proc_LOG_20USE_20LOG;
extern unsigned char D_proc_STRESS_20TEST[];
void proc_STRESS_20TEST( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_STRESS_20TEST);
	if (!ctx->doingAbort) {
		Long v0;
		Long v1;
		Long lProcessID__i;
		Long v2;
		Long lelement__i;
		Long li;
		Long v3;
		Txt lProcessID__t;
		Long lStackSize__i;
		Value_array_text lLogs__at;
		Long lLoops__i;
		Long lWindowID__i;
		{
			Bool t0;
			c.f.fLine=17;
			if (g->Call(ctx,(PCV[]){t0.cv()},0,492)) goto _0;
			if (!(t0.get())) goto _2;
		}
		lLoops__i=1000000;
		goto _3;
_2:
		lLoops__i=1000;
_3:
		lStackSize__i=0;
		{
			Long t1;
			t1=inNbExplicitParam;
			if (1!=t1.get()) goto _4;
		}
		{
			Ref t3;
			t3.setLocalRef(ctx,lLogs__at.cv());
			c.f.fLine=28;
			if (g->Call(ctx,(PCV[]){nullptr,t3.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ref t4;
			t4.setLocalRef(ctx,lLogs__at.cv());
			c.f.fLine=30;
			if (g->Call(ctx,(PCV[]){nullptr,t4.cv(),KSun.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t5;
			t5.setLocalRef(ctx,lLogs__at.cv());
			c.f.fLine=31;
			if (g->Call(ctx,(PCV[]){nullptr,t5.cv(),KMon.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t6;
			t6.setLocalRef(ctx,lLogs__at.cv());
			c.f.fLine=32;
			if (g->Call(ctx,(PCV[]){nullptr,t6.cv(),KTue.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t7;
			t7.setLocalRef(ctx,lLogs__at.cv());
			c.f.fLine=33;
			if (g->Call(ctx,(PCV[]){nullptr,t7.cv(),KWed.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t8;
			t8.setLocalRef(ctx,lLogs__at.cv());
			c.f.fLine=34;
			if (g->Call(ctx,(PCV[]){nullptr,t8.cv(),KThu.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t9;
			t9.setLocalRef(ctx,lLogs__at.cv());
			c.f.fLine=35;
			if (g->Call(ctx,(PCV[]){nullptr,t9.cv(),KFri.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t10;
			t10.setLocalRef(ctx,lLogs__at.cv());
			c.f.fLine=36;
			if (g->Call(ctx,(PCV[]){nullptr,t10.cv(),KSat.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		{
			Bool t11;
			t11=Bool(1).get();
			Bool t12;
			c.f.fLine=39;
			proc_LOG_20ENABLE(glob,ctx,1,1,(PCV[]){t11.cv()},t12.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		{
			Long t13;
			c.f.fLine=40;
			if (g->Call(ctx,(PCV[]){t13.cv()},0,322)) goto _0;
			g->Check(ctx);
			lProcessID__i=t13.get();
		}
		{
			Txt t14;
			c.f.fLine=43;
			if (g->Call(ctx,(PCV[]){t14.cv(),lProcessID__i.cv()},1,10)) goto _0;
			lProcessID__t=t14.get();
		}
		li=1;
		v0=lLoops__i.get();
		goto _5;
_7:
		{
			Long t15;
			c.f.fLine=47;
			if (g->Call(ctx,(PCV[]){t15.cv()},0,100)) goto _0;
			Long t16;
			t16=t15.get()%7;
			lelement__i=t16.get();
		}
		{
			Bool t17;
			t17=1<=lelement__i.get();
			Bool t18;
			t18=7>=lelement__i.get();
			if (!( t17.get()&&t18.get())) goto _9;
		}
		{
			Txt t20;
			c.f.fLine=50;
			t20=lLogs__at.arrayElem(ctx,lelement__i.get()).get();
			Txt t21;
			t21=t20.get();
			proc_LOG_20USE_20LOG(glob,ctx,1,1,(PCV[]){t21.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		if (ctx->doingAbort) goto _0;
		goto _10;
_9:
		{
			Txt t22;
			c.f.fLine=52;
			proc_LOG_20USE_20LOG(glob,ctx,0,1,(PCV[]){t22.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
_10:
		{
			Long t23;
			t23=lLoops__i.get()%100;
			if (0!=t23.get()) goto _11;
		}
		{
			Txt t25;
			c.f.fLine=56;
			proc_LOG_20CLOSE_20LOG(glob,ctx,0,1,(PCV[]){t25.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
_11:
		{
			Txt t26;
			c.f.fLine=60;
			if (g->Call(ctx,(PCV[]){t26.cv(),li.cv()},1,10)) goto _0;
			Txt t27;
			t27=Parm<Txt>(ctx,inParams,inNbParam,1).get();
			Txt t28;
			t28=lProcessID__t.get();
			proc_LOG_20ADD_20ENTRY(glob,ctx,3,3,(PCV[]){t28.cv(),t26.cv(),t27.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		if (ctx->doingAbort) goto _0;
_6:
		li=li.get()+1;
_5:
		if (li.get()<=v0.get()) goto _7;
_8:
		{
			Long t32;
			t32=0;
			Bool t34;
			t34=Bool(0).get();
			Txt t31;
			Txt t35;
			t35=KFinished.get();
			c.f.fLine=66;
			proc_LOG_20BUG_20ALERT(glob,ctx,1,4,(PCV[]){t35.cv(),t31.cv(),t32.cv(),t34.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		goto _12;
_4:
		li=1;
		v2=5;
		goto _13;
_15:
		{
			Txt t36;
			c.f.fLine=71;
			if (g->Call(ctx,(PCV[]){t36.cv()},0,684)) goto _0;
			g->Check(ctx);
			Txt t37;
			g->AddString(t36.get(),K_20.get(),t37.get());
			Txt t38;
			if (g->Call(ctx,(PCV[]){t38.cv(),li.cv(),K000.cv()},2,10)) goto _0;
			g->AddString(t37.get(),t38.get(),Parm<Txt>(ctx,inParams,inNbParam,1).get());
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t40;
			c.f.fLine=72;
			if (g->Call(ctx,(PCV[]){t40.cv()},0,684)) goto _0;
			g->Check(ctx);
			Long t41;
			if (g->Call(ctx,(PCV[]){t41.cv(),t40.cv(),lStackSize__i.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv()},4,317)) goto _0;
			lProcessID__i=t41.get();
		}
		if (ctx->doingAbort) goto _0;
_14:
		li=li.get()+1;
_13:
		if (li.get()<=v2.get()) goto _15;
_16:
_12:
_0:
_1:
;
	}

}
