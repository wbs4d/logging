extern Txt KLOG_20CLOSE_20LOG2;
extern Txt KLog_20Writer;
extern unsigned char D_proc_LOG_20CLOSE_20LOG[];
void proc_LOG_20CLOSE_20LOG( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20CLOSE_20LOG);
	if (!ctx->doingAbort) {
		{
			Long t0;
			t0=inNbExplicitParam;
			if (1!=t0.get()) goto _2;
		}
		c.f.fLine=20;
		if (g->Call(ctx,(PCV[]){nullptr,KLog_20Writer.cv(),KLOG_20CLOSE_20LOG2.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv()},3,1389)) goto _0;
		g->Check(ctx);
		if (ctx->doingAbort) goto _0;
		goto _3;
_2:
		c.f.fLine=22;
		if (g->Call(ctx,(PCV[]){nullptr,KLog_20Writer.cv(),KLOG_20CLOSE_20LOG2.cv()},2,1389)) goto _0;
		g->Check(ctx);
_3:
_0:
_1:
;
	}

}
