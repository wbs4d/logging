extern Txt Klog;
extern Txt KmaxLogSize;
Asm4d_Proc proc_LOG__INIT;
extern unsigned char D_proc_LOG_20MAXIMUM_20SIZE[];
void proc_LOG_20MAXIMUM_20SIZE( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20MAXIMUM_20SIZE);
	if (!ctx->doingAbort) {
		Obj l__4D__auto__mutex__0;
		new ( outResult) Long();
		c.f.fLine=23;
		proc_LOG__INIT(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Long t0;
			t0=inNbExplicitParam;
			if (1!=t0.get()) goto _2;
		}
		{
			Obj t2;
			c.f.fLine=26;
			if (g->Call(ctx,(PCV[]){t2.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t3;
			if (g->GetMember(ctx,t2.cv(),Klog.cv(),t3.cv())) goto _0;
			Obj t4;
			if (g->Call(ctx,(PCV[]){t4.cv(),t3.cv()},1,1529)) goto _0;
			l__4D__auto__mutex__0=t4.get();
		}
		{
			Obj t5;
			c.f.fLine=27;
			if (g->Call(ctx,(PCV[]){t5.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t6;
			if (g->GetMember(ctx,t5.cv(),Klog.cv(),t6.cv())) goto _0;
			if (g->SetMember(ctx,t6.cv(),KmaxLogSize.cv(),Parm<Long>(ctx,inParams,inNbParam,1).cv())) goto _0;
		}
		if (ctx->doingAbort) goto _0;
		{
			Obj t7;
			l__4D__auto__mutex__0=t7.get();
		}
_2:
		{
			Obj t8;
			c.f.fLine=31;
			if (g->Call(ctx,(PCV[]){t8.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t9;
			if (g->GetMember(ctx,t8.cv(),Klog.cv(),t9.cv())) goto _0;
			Variant t10;
			if (g->GetMember(ctx,t9.cv(),KmaxLogSize.cv(),t10.cv())) goto _0;
			Long t11;
			if (!g->GetValue(ctx,(PCV[]){t11.cv(),t10.cv(),nullptr})) goto _0;
			Res<Long>(outResult)=t11.get();
		}
		goto _0;
_0:
_1:
;
	}

}
