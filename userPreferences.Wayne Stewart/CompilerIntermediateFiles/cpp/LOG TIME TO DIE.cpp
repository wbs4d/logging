Asm4d_Proc proc_LOG_20CLOSE_20LOG2;
extern unsigned char D_proc_LOG_20TIME_20TO_20DIE[];
void proc_LOG_20TIME_20TO_20DIE( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20TIME_20TO_20DIE);
	if (!ctx->doingAbort) {
		{
			Txt t0;
			c.f.fLine=19;
			proc_LOG_20CLOSE_20LOG2(glob,ctx,0,1,(PCV[]){t0.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		c.f.fLine=21;
		if (g->Call(ctx,(PCV[]){nullptr},0,1390)) goto _0;
		g->Check(ctx);
_0:
_1:
;
	}

}
