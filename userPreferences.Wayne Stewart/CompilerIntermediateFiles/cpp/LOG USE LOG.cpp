extern int32_t vlg;
extern Txt KdefaultLog;
extern Txt Kfile;
extern Txt KfileName;
extern Txt Kfolder;
extern Txt KfolderPath;
extern Txt Kk;
extern Txt Klogs;
extern Txt KlogsFolder;
Asm4d_Proc proc_LOG_20DECLARE_20LOG;
extern unsigned char D_proc_LOG_20USE_20LOG[];
void proc_LOG_20USE_20LOG( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20USE_20LOG);
	if (!ctx->doingAbort) {
		Obj lTemp__o;
		{
			Long t0;
			t0=inNbExplicitParam;
			if (1!=t0.get()) goto _2;
		}
		{
			Long t2;
			c.f.fLine=23;
			t2=Parm<Txt>(ctx,inParams,inNbParam,1).get().fLength;
			if (0>=t2.get()) goto _3;
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t5;
			Txt t4;
			Txt t6;
			c.f.fLine=24;
			t6=Parm<Txt>(ctx,inParams,inNbParam,1).get();
			proc_LOG_20DECLARE_20LOG(glob,ctx,1,3,(PCV[]){t6.cv(),t4.cv(),t5.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		if (ctx->doingAbort) goto _0;
		{
			Obj t7;
			c.f.fLine=26;
			if (g->Call(ctx,(PCV[]){t7.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t8;
			if (g->GetMember(ctx,t7.cv(),Klogs.cv(),t8.cv())) goto _0;
			Variant t9;
			if (g->GetMember(ctx,t8.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),t9.cv())) goto _0;
			Obj t10;
			if (!g->GetValue(ctx,(PCV[]){t10.cv(),t9.cv(),nullptr})) goto _0;
			lTemp__o=t10.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t11;
			c.f.fLine=27;
			if (g->GetMember(ctx,lTemp__o.cv(),KfileName.cv(),t11.cv())) goto _0;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),t11.cv())) goto _0;
		}
		{
			Variant t12;
			c.f.fLine=28;
			if (g->GetMember(ctx,lTemp__o.cv(),KfolderPath.cv(),t12.cv())) goto _0;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfolder.cv(),t12.cv())) goto _0;
		}
		goto _4;
_3:
		{
			Obj t13;
			c.f.fLine=31;
			if (g->Call(ctx,(PCV[]){t13.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t14;
			if (g->GetMember(ctx,t13.cv(),Kk.cv(),t14.cv())) goto _0;
			Variant t15;
			if (g->GetMember(ctx,t14.cv(),KdefaultLog.cv(),t15.cv())) goto _0;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),t15.cv())) goto _0;
		}
		{
			Obj t16;
			c.f.fLine=32;
			if (g->Call(ctx,(PCV[]){t16.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t17;
			if (g->GetMember(ctx,t16.cv(),Kk.cv(),t17.cv())) goto _0;
			Variant t18;
			if (g->GetMember(ctx,t17.cv(),KlogsFolder.cv(),t18.cv())) goto _0;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfolder.cv(),t18.cv())) goto _0;
		}
_4:
		goto _5;
_2:
		{
			Obj t19;
			c.f.fLine=37;
			if (g->Call(ctx,(PCV[]){t19.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t20;
			if (g->GetMember(ctx,t19.cv(),Kk.cv(),t20.cv())) goto _0;
			Variant t21;
			if (g->GetMember(ctx,t20.cv(),KdefaultLog.cv(),t21.cv())) goto _0;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfile.cv(),t21.cv())) goto _0;
		}
		{
			Obj t22;
			c.f.fLine=38;
			if (g->Call(ctx,(PCV[]){t22.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t23;
			if (g->GetMember(ctx,t22.cv(),Kk.cv(),t23.cv())) goto _0;
			Variant t24;
			if (g->GetMember(ctx,t23.cv(),KlogsFolder.cv(),t24.cv())) goto _0;
			if (g->SetMember(ctx,Var<Obj>(ctx,vlg).cv(),Kfolder.cv(),t24.cv())) goto _0;
		}
_5:
_0:
_1:
;
	}

}
