extern int32_t vLog__DocRefs__ah;
extern int32_t vLog__Paths__at;
extern int32_t vOK;
extern Txt K;
extern Txt KTEXT;
extern Txt K_2D;
extern Txt K_2E;
extern Txt K_3A;
extern Txt K__;
extern Txt Klog;
extern Txt KmaxLogSize;
extern Txt kI_UxMf2Gtu0;
Asm4d_Proc proc_LOGUTIL__POSITIONR;
Asm4d_Proc proc_LOG_20COMPRESS;
Asm4d_Proc proc_LOG__INIT;
extern unsigned char D_proc_LOG_20THIS[];
void proc_LOG_20THIS( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20THIS);
	if (!ctx->doingAbort) {
		Txt larchivedfileName__t;
		Long lPosition__i;
		Txt lextension__t;
		Long ldot__i;
		Txt lPathToLogFile__t;
		Time ldocRef__h;
		Txt lTS__t;
		Txt larchivedfilePath__t;
		Txt lErrorHandler__t;
		c.f.fLine=27;
		proc_LOG__INIT(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Txt t0;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t0.cv()},0,704)) goto _0;
			g->Check(ctx);
			lErrorHandler__t=t0.get();
		}
		c.f.fLine=31;
		if (g->Call(ctx,(PCV[]){nullptr,kI_UxMf2Gtu0.cv()},1,155)) goto _0;
		g->Check(ctx);
		c.f.fLine=32;
		{
			Long t1;
			if (g->Call(ctx,(PCV[]){t1.cv(),Parm<Txt>(ctx,inParams,inNbParam,3).cv(),lPathToLogFile__t.cv()},2,15)) goto _0;
			if (0!=t1.get()) goto _2;
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=33;
		g->AddString(Parm<Txt>(ctx,inParams,inNbParam,2).get(),Parm<Txt>(ctx,inParams,inNbParam,3).get(),lPathToLogFile__t.get());
		if (ctx->doingAbort) goto _0;
_2:
		{
			Long t4;
			c.f.fLine=36;
			if (g->Call(ctx,(PCV[]){t4.cv(),Ref(ctx,vLog__Paths__at).cv(),lPathToLogFile__t.cv()},2,230)) goto _0;
			lPosition__i=t4.get();
		}
		if (0>=lPosition__i.get()) goto _3;
		c.f.fLine=38;
		ldocRef__h=Var<Value_array_time>(ctx,vLog__DocRefs__ah).arrayElem(ctx,lPosition__i.get()).get();
		if (ctx->doingAbort) goto _0;
		goto _4;
_3:
		{
			Long t6;
			c.f.fLine=42;
			if (g->Call(ctx,(PCV[]){t6.cv(),lPathToLogFile__t.cv()},1,476)) goto _0;
			g->Check(ctx);
			if (1!=t6.get()) goto _5;
		}
		{
			Time t8;
			c.f.fLine=43;
			if (g->Call(ctx,(PCV[]){t8.cv(),lPathToLogFile__t.cv()},1,265)) goto _0;
			g->Check(ctx);
			ldocRef__h=t8.get();
		}
		goto _6;
_5:
		c.f.fLine=45;
		{
			Long t9;
			if (g->Call(ctx,(PCV[]){t9.cv(),Parm<Txt>(ctx,inParams,inNbParam,2).cv()},1,476)) goto _0;
			g->Check(ctx);
			if (0==t9.get()) goto _7;
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=46;
		if (g->Call(ctx,(PCV[]){nullptr,Parm<Txt>(ctx,inParams,inNbParam,2).cv(),Ref((optyp)3).cv()},2,475)) goto _0;
		g->Check(ctx);
		if (ctx->doingAbort) goto _0;
_7:
		{
			Time t11;
			c.f.fLine=48;
			if (g->Call(ctx,(PCV[]){t11.cv(),lPathToLogFile__t.cv(),KTEXT.cv()},2,266)) goto _0;
			g->Check(ctx);
			ldocRef__h=t11.get();
		}
_6:
		c.f.fLine=52;
		if (g->Call(ctx,(PCV[]){nullptr,Ref(ctx,vLog__Paths__at).cv(),lPathToLogFile__t.cv()},2,911)) goto _0;
		g->Check(ctx);
		{
			Time t12;
			t12=ldocRef__h.get();
			c.f.fLine=53;
			if (g->Call(ctx,(PCV[]){nullptr,Ref(ctx,vLog__DocRefs__ah).cv(),t12.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		{
			Long t13;
			t13=Var<Value_array_time>(ctx,vLog__DocRefs__ah).size();
			lPosition__i=t13.get();
		}
_4:
		if (1!=Var<Long>(ctx,vOK).get()) goto _8;
		c.f.fLine=59;
		{
			Time t15;
			t15=ldocRef__h.get();
			if (g->Call(ctx,(PCV[]){nullptr,t15.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv()},2,103)) goto _0;
			g->Check(ctx);
		}
		if (ctx->doingAbort) goto _0;
		{
			Obj t16;
			c.f.fLine=60;
			if (g->Call(ctx,(PCV[]){t16.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t17;
			if (g->GetMember(ctx,t16.cv(),Klog.cv(),t17.cv())) goto _0;
			Variant t18;
			if (g->GetMember(ctx,t17.cv(),KmaxLogSize.cv(),t18.cv())) goto _0;
			Bool t19;
			if (g->OperationOnAny(ctx,7,t18.cv(),Num(0).cv(),t19.cv())) goto _0;
			Num t20;
			if (g->Call(ctx,(PCV[]){t20.cv(),lPathToLogFile__t.cv()},1,479)) goto _0;
			Obj t21;
			if (g->Call(ctx,(PCV[]){t21.cv()},0,1525)) goto _0;
			Variant t22;
			if (g->GetMember(ctx,t21.cv(),Klog.cv(),t22.cv())) goto _0;
			Variant t23;
			if (g->GetMember(ctx,t22.cv(),KmaxLogSize.cv(),t23.cv())) goto _0;
			Bool t24;
			if (g->OperationOnAny(ctx,5,t20.cv(),t23.cv(),t24.cv())) goto _0;
			Bool t25;
			t25=t19.get()&&t24.get();
			if (!(t25.get())) goto _9;
		}
		{
			Time t26;
			t26=ldocRef__h.get();
			c.f.fLine=62;
			if (g->Call(ctx,(PCV[]){nullptr,t26.cv()},1,267)) goto _0;
			g->Check(ctx);
		}
		{
			Txt t27;
			c.f.fLine=64;
			t27=Parm<Txt>(ctx,inParams,inNbParam,3).get();
			Txt t28;
			t28=K_2E.get();
			Long t29;
			proc_LOGUTIL__POSITIONR(glob,ctx,2,2,(PCV[]){t28.cv(),t27.cv()},t29.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			ldot__i=t29.get();
		}
		if (ctx->doingAbort) goto _0;
		if (0!=ldot__i.get()) goto _10;
		lextension__t=K.get();
		c.f.fLine=67;
		larchivedfileName__t=Parm<Txt>(ctx,inParams,inNbParam,3).get();
		if (ctx->doingAbort) goto _0;
		goto _11;
_10:
		{
			Long t31;
			t31=ldot__i.get()+1;
			c.f.fLine=69;
			Txt t32;
			if (g->Call(ctx,(PCV[]){t32.cv(),Parm<Txt>(ctx,inParams,inNbParam,3).cv(),t31.cv()},2,12)) goto _0;
			lextension__t=t32.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Long t33;
			t33=ldot__i.get()-1;
			c.f.fLine=70;
			Txt t34;
			if (g->Call(ctx,(PCV[]){t34.cv(),Parm<Txt>(ctx,inParams,inNbParam,3).cv(),Long(1).cv(),t33.cv()},3,12)) goto _0;
			larchivedfileName__t=t34.get();
		}
		if (ctx->doingAbort) goto _0;
_11:
		{
			Txt t35;
			c.f.fLine=73;
			if (g->Call(ctx,(PCV[]){t35.cv()},0,1445)) goto _0;
			Txt t36;
			if (g->Call(ctx,(PCV[]){t36.cv(),t35.cv(),K_3A.cv(),K.cv()},3,233)) goto _0;
			lTS__t=t36.get();
		}
		{
			Txt t37;
			c.f.fLine=74;
			if (g->Call(ctx,(PCV[]){t37.cv(),lTS__t.cv(),K_2D.cv(),K.cv()},3,233)) goto _0;
			lTS__t=t37.get();
		}
		{
			Txt t38;
			c.f.fLine=75;
			if (g->Call(ctx,(PCV[]){t38.cv(),lTS__t.cv(),Long(1).cv(),Long(15).cv()},3,12)) goto _0;
			lTS__t=t38.get();
		}
		{
			Txt t39;
			g->AddString(larchivedfileName__t.get(),K__.get(),t39.get());
			Txt t40;
			g->AddString(t39.get(),lTS__t.get(),t40.get());
			g->AddString(t40.get(),lextension__t.get(),larchivedfileName__t.get());
		}
		c.f.fLine=78;
		g->AddString(Parm<Txt>(ctx,inParams,inNbParam,2).get(),larchivedfileName__t.get(),larchivedfilePath__t.get());
		if (ctx->doingAbort) goto _0;
		c.f.fLine=80;
		if (g->Call(ctx,(PCV[]){nullptr,lPathToLogFile__t.cv(),larchivedfilePath__t.cv()},2,540)) goto _0;
		g->Check(ctx);
		{
			Time t43;
			c.f.fLine=82;
			if (g->Call(ctx,(PCV[]){t43.cv(),lPathToLogFile__t.cv(),KTEXT.cv()},2,266)) goto _0;
			g->Check(ctx);
			ldocRef__h=t43.get();
		}
		c.f.fLine=83;
		Var<Value_array_time>(ctx,vLog__DocRefs__ah).arrayElem(ctx,lPosition__i.get())=ldocRef__h.get();
		Touch(ctx,vLog__DocRefs__ah);
		if (ctx->doingAbort) goto _0;
		{
			Bool t45;
			t45=Bool(0).get();
			Txt t46;
			t46=larchivedfilePath__t.get();
			c.f.fLine=84;
			proc_LOG_20COMPRESS(glob,ctx,1,2,(PCV[]){t46.cv(),t45.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
_9:
_8:
		c.f.fLine=92;
		if (g->Call(ctx,(PCV[]){nullptr,lErrorHandler__t.cv()},1,155)) goto _0;
		g->Check(ctx);
_0:
_1:
;
	}

}
