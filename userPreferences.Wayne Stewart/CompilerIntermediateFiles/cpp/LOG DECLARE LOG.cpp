extern Txt K_2Etxt;
extern Txt KfileName;
extern Txt KfolderPath;
extern Txt Kk;
extern Txt Klogs;
extern Txt KlogsFolder;
Asm4d_Proc proc_LOG__INIT;
extern unsigned char D_proc_LOG_20DECLARE_20LOG[];
void proc_LOG_20DECLARE_20LOG( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_LOG_20DECLARE_20LOG);
	if (!ctx->doingAbort) {
		Obj lLog__o;
		Obj l__4D__auto__mutex__0;
		Long lParameters__i;
		c.f.fLine=20;
		proc_LOG__INIT(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Long t0;
			t0=inNbExplicitParam;
			lParameters__i=t0.get();
		}
		if (1!=lParameters__i.get()) goto _3;
		c.f.fLine=29;
		g->AddString(Parm<Txt>(ctx,inParams,inNbParam,1).get(),K_2Etxt.get(),Parm<Txt>(ctx,inParams,inNbParam,2).get());
		if (ctx->doingAbort) goto _0;
		{
			Obj t3;
			c.f.fLine=30;
			if (g->Call(ctx,(PCV[]){t3.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t4;
			if (g->GetMember(ctx,t3.cv(),Kk.cv(),t4.cv())) goto _0;
			Variant t5;
			if (g->GetMember(ctx,t4.cv(),KlogsFolder.cv(),t5.cv())) goto _0;
			Txt t6;
			if (!g->GetValue(ctx,(PCV[]){t6.cv(),t5.cv(),nullptr})) goto _0;
			Parm<Txt>(ctx,inParams,inNbParam,3)=t6.get();
		}
		if (ctx->doingAbort) goto _0;
		goto _2;
_3:
		if (2!=lParameters__i.get()) goto _4;
		{
			Obj t8;
			c.f.fLine=33;
			if (g->Call(ctx,(PCV[]){t8.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t9;
			if (g->GetMember(ctx,t8.cv(),Kk.cv(),t9.cv())) goto _0;
			Variant t10;
			if (g->GetMember(ctx,t9.cv(),KlogsFolder.cv(),t10.cv())) goto _0;
			Txt t11;
			if (!g->GetValue(ctx,(PCV[]){t11.cv(),t10.cv(),nullptr})) goto _0;
			Parm<Txt>(ctx,inParams,inNbParam,3)=t11.get();
		}
		if (ctx->doingAbort) goto _0;
		goto _2;
_4:
_2:
		{
			Obj t12;
			c.f.fLine=39;
			if (g->Call(ctx,(PCV[]){t12.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t13;
			if (g->GetMember(ctx,t12.cv(),Klogs.cv(),t13.cv())) goto _0;
			Variant t14;
			if (g->GetMember(ctx,t13.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),t14.cv())) goto _0;
			Bool t15;
			if (g->OperationOnAny(ctx,6,t14.cv(),Value_null().cv(),t15.cv())) goto _0;
			if (!(t15.get())) goto _5;
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=40;
		{
			Obj t16;
			if (g->Call(ctx,(PCV[]){t16.cv(),KfolderPath.cv(),Parm<Txt>(ctx,inParams,inNbParam,3).cv(),KfileName.cv(),Parm<Txt>(ctx,inParams,inNbParam,2).cv()},4,1526)) goto _0;
			g->Check(ctx);
			lLog__o=t16.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Obj t17;
			c.f.fLine=42;
			if (g->Call(ctx,(PCV[]){t17.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t18;
			if (g->GetMember(ctx,t17.cv(),Klogs.cv(),t18.cv())) goto _0;
			Obj t19;
			if (g->Call(ctx,(PCV[]){t19.cv(),t18.cv()},1,1529)) goto _0;
			l__4D__auto__mutex__0=t19.get();
		}
		{
			Obj t20;
			c.f.fLine=43;
			if (g->Call(ctx,(PCV[]){t20.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t21;
			if (g->GetMember(ctx,t20.cv(),Klogs.cv(),t21.cv())) goto _0;
			if (g->SetMember(ctx,t21.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),lLog__o.cv())) goto _0;
		}
		if (ctx->doingAbort) goto _0;
		{
			Obj t22;
			l__4D__auto__mutex__0=t22.get();
		}
_5:
_0:
_1:
;
	}

}
